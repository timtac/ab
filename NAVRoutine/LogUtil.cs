﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NAVRoutine
{
    public class LogUtil
    {
        public string FilePathLOG
        {
            get;
            set;
        }

        public void Logger(String FileName, String ErrorMessage, string extension = ".txt")
        {
            string LogFile = this.FilePathLOG;

            // dd/mm/yyyy hh:mm:ss AM/PM ==> Log Message
            String sLogFormat = DateTime.Now.ToShortDateString().ToString() + " " + DateTime.Now.ToLongTimeString().ToString() + " ==> ";

            String sErrorTime = DateTime.Now.ToString("yyyyMMdd");// sYear + sMonth + sDay;

            StreamWriter sw = new StreamWriter(LogFile + FileName + sErrorTime + extension, true);
            sw.WriteLine(sLogFormat + ErrorMessage);
            sw.Flush();
            sw.Close();
        }
    }
}
