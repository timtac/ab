﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace NAVRoutine
{
    public class LeaveApplicationViewModel : LeaveInformation
    {
        #region ------------ Properties -----------------------

        public string ApplicationNo { get; set; }

        [DataType(DataType.Date)]
        public string ApplicationDate { get; set; }

        [Required]
        [Display(Name = "Employee No")]
        public string EmployeeNo { get; set; }

        [Display(Name = "Employee Name")]
        public string EmployeeName { get; set; }

        [Display(Name = "Job Title")]
        public string JobTitle { get; set; }

        public string Branch { get; set; }

        public string Department { get; set; }

        [Display(Name = "Supervisor Name")]
        public string SupervisorName { get; set; }

        [Display(Name = "Supervisor Email")]
        public string SupervisorEmail { get; set; }

        [Required]
        [Display(Name = "Leave Type")]
        public string LeaveType { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Start Date")]
        public string LeaveStartDate { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "End Date")]
        public string LeaveEndDate { get; set; }

        // To be autoopulated.
        [Range(0, 365, ErrorMessage = "Enter a value less than the allowed leave balance.")]
        [StringLength(3, ErrorMessage = "Maximum of 3 digits.")]
        [Display(Name = "Days Applied For")]
        public string LeaveDaysAppliedFor { get; set; }

        [Required]
        [Range(0, 365, ErrorMessage = "Enter a value less than the appllied leave days.")]
        [StringLength(3, ErrorMessage = "Maximum of 3 digits.")]
        [Display(Name = "Days Appproved.")]
        public string LeaveDaysApproved { get; set; }

        [StringLength(2, ErrorMessage = "Maximum of 2 digits.")]
        [Display(Name = "Total Annual Leave(Days)")]
        public string TotalAnnualLeaveDays { get; set; }

        [StringLength(2, ErrorMessage = "Maximum of 2 digits.")]
        [Display(Name = "Annual Leave Taken(Days)")]
        public string TotalAnnualLeaveTaken { get; set; }

        [StringLength(2, ErrorMessage = "Maximum of 2 digits.")]
        [Display(Name = "Annual Balance(Days)")]
        public string TotalAnnualLeaveBalance { get; set; }

        [Required]
        [Display(Name = "Reliever Code/Name")]
        public string RelieverCode { get; set; }

        [Display(Name = "Reliever Name")]
        public string RelieverName { get; set; }

        [Display(Name = "Application Status")]
        public string LeaveApplicationStatus { get; set; }

        [Required]
        [Display(Name = "Rejection Reason")]
        public string RejectionReason { get; set; }

        // used to control certain parts of the page to make editable or not. eg. Rejection Reason and Leave days approved.
        public bool IsSupervisor { get; set; }

        // This fields will be hidden and used to inform the nav wether to save the leave application or send it for approaval.
        // Saving means setting the status flag to NEW while sending it for approval will set the status flag to PENDING APPROVAL.
        public bool SubmitLeave { get; set; }

        public bool Approved { get; set; }

        public IEnumerable<SelectListItem> Employees { set; get; }

        #endregion --------------------------------------------

    }
}
