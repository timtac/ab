﻿using NAVRoutine.EmployeeRef;
using NAVRoutine.JobApplicationRef;
using NAVRoutine.JobApplicationRefereeRef;
using NAVRoutine.JobApplicationQualificationRef;
using NAVRoutine.JobApplicationEmpHistoryRef;
using NAVRoutine.ApplicationAttachmentRef;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace NAVRoutine
{
    public class JobApplicationDetail : RequestResponse
    {
        public JobApplication JobApplication { get; set; }
        public List<JobApplicationReferee> JobApplicationReferees { get; set; }
        public List<JobApplicationQualification> JobApplicationQualifications { get; set; }
        public List<JobApplicationEmpHistory> JobApplicationEmpHistorys { get; set; }
        public List<ApplicationAttachment> ApplicationAttachments { get; set; }

        public JobApplicationDetail()
        {
            JobApplication = new JobApplication();
            JobApplicationEmpHistorys = new List<JobApplicationEmpHistory>();
            JobApplicationQualifications = new List<JobApplicationQualification>();
            JobApplicationReferees = new List<JobApplicationReferee>();
            ApplicationAttachments = new List<ApplicationAttachment>();
        }
    }

    public class ApplyViewModel
    {
        public ApplicantBioData ApplicantBioData { get; set; }
        public List<ApplicantReferee> Referees { get; set; }
        public List<ApplicantQualification> Qualifications { get; set; }
        public List<ApplicantEmploymentHistory> EmploymentHistory { get; set; }

        // If true, the current information passed with the form will be used to update the user boidata.
        [Display(Name ="Check  this box to update your profile information with the infomraion")]
        public bool UpdateBiodata { get; set; }

        // The number of the job being applied for on the main system.
        public string JobApplicationNo { get; set; }
        public string JobPosition { get; set; }

        public ApplyViewModel()
        {
            ApplicantBioData = new ApplicantBioData();
            Referees = new List<ApplicantReferee>();
            Qualifications = new List<ApplicantQualification>();
            EmploymentHistory = new List<ApplicantEmploymentHistory>();
            UpdateBiodata = false;
        }
    }

    public class ApplicantBioData
    {
        [Required]
        [Display(Name = "Title *")]
        public string Title { get; set; }

        [Display(Name = "First Name *")]
        [Required]
        public string FirstName { get; set; }

        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }

        [Required]
        [Display(Name = "Last Name *")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "ID Number *")]
        public string IdNo { get; set; }

        [Required]
        [Display(Name = "Citizenship *")]
        public string Citizenship { get; set; }

        public string Gender { get; set; }

        [Display(Name = "Date Of Birth")]
        [DataType(DataType.Date)]
        public string DateOfBirth { get; set; }

        [Display(Name = "Marital Status")]
        public string MaritalStatus { get; set; }

        [Range(0.00,1000000000.00, ErrorMessage ="Value should be in the range 0-1000000000")]
        [Display(Name ="Asking Basic Pay(Per Annum.)")]
        public string AskingBasicPay { get; set; }

        [Display(Name = "Cell Phone No")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^[0-9]{1,12}$", ErrorMessage = "Only digits allowed.")]
        public string CellPhoneNo { get; set; }

        [Display(Name = "Home Phone Number")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^[0-9]{1,12}$", ErrorMessage = "Only digits allowed.")]
        public string HomePhoneNumber { get; set; }

        [Display(Name = "Residential Address (Street, No.)")]
        public string ResidentialAddressStreet { get; set; }

        [Display(Name = "Residential Address (District)")]
        public string ResidentialAddressDistrict { get; set; }

        [Display(Name = "Residential Address (City)")]
        public string ResidentialAddressCity { get; set; }

        public string Country { get; set; }

        [Display(Name = "Postal Address")]
        public string PostalAddress { get; set; }

        public string Language { get; set; }


        // Job applied to.
        public string JobApplicationNo { get; set; }
        public DateTime DateApplied { get; set; }
        public Applicant_Type ApplicantType { get; set; }
        public string JobRequestNo { get; set; }
        public string PositionAppliedForCode { get; set; }
        public string EmployeeNo { get; set; }
        public bool IsEmployee { get; set; }

        public string Age { get; set; }
       
        [Display(Name = "Ethnic Origin")]
        public string EthnicOrigin { get; set; }

        [Display(Name = "Work Phone No")]
        public string WorkPhoneNo { get; set; }

        [Display(Name = "E-mail")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public IEnumerable<SelectListItem> GenderSelectListItems
        {
            get
            {
                foreach (JobApplicationRef.Gender type in Enum.GetValues(typeof(JobApplicationRef.Gender)))
                {
                    SelectListItem selectListItem = new SelectListItem();
                    selectListItem.Text = type.ToString();
                    selectListItem.Value = type.ToString();
                    selectListItem.Selected = Gender == type.ToString();
                    yield return selectListItem;
                }
            }
        }

        public IEnumerable<SelectListItem> MaritalStatusSelectListItems
        {
            get
            {
                foreach (JobApplicationRef.Marital_Status type in Enum.GetValues(typeof(JobApplicationRef.Marital_Status)))
                {
                    SelectListItem selectListItem = new SelectListItem();
                    selectListItem.Text = type.ToString();
                    selectListItem.Value = type.ToString();
                    selectListItem.Selected = MaritalStatus == type.ToString();
                    yield return selectListItem;
                }
            }
        }

        public IEnumerable<SelectListItem> TitleSelectListItems
        {
            get
            {
                foreach (JobApplicationRef.Title type in Enum.GetValues(typeof(JobApplicationRef.Title)))
                {
                    SelectListItem selectListItem = new SelectListItem();
                    selectListItem.Text = type.ToString();
                    selectListItem.Value = type.ToString();
                    selectListItem.Selected = Title == type.ToString();
                    yield return selectListItem;
                }
            }
        }

        public IEnumerable<SelectListItem> EthnicOriginSelectListItems
        {
            get
            {
                foreach (JobApplicationRef.Ethnic_Origin type in Enum.GetValues(typeof(JobApplicationRef.Ethnic_Origin)))
                {
                    SelectListItem selectListItem = new SelectListItem();
                    selectListItem.Text = type.ToString();
                    selectListItem.Value = type.ToString();
                    selectListItem.Selected = EthnicOrigin == type.ToString();
                    yield return selectListItem;
                }
            }
        }

        public IEnumerable<SelectListItem> CountriesSelectListItems
        {
            get;
            set;
        }
    }

    public class ApplicantReferee
    {
        // Use internally to track the application the record is attached to.
        public string ApplicationNo { get; set; }

        [Required]
        public string Designation { get; set; }

        [Required]
        public string Names { get; set; }

        public string Institution { get; set; }

        public string Address { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        [Display(Name ="Phone Number")]
        public string TelephoneNo { get; set; }

        [DataType(DataType.EmailAddress)]
        [Display(Name ="E Mail")]
        public string EMail { get; set; }
    }

    public class ApplicantQualification
    {
        // Use internally to track the application the record is attached to.
        public string ApplicationNo { get; set; }

        [Required]
        [Display(Name = "Qualification Type *")]
        public string  QualificationType { get; set; }

        [Required]
        [Display(Name = "Qualification Code *")]
        public string  QualificationCode { get; set; }

        [Display(Name ="Institution *")]
        public string Institution { get; set; }

        [Required]
        [Display(Name = "From Date *")]
        [DataType(DataType.Date)]
        public string FromDate { get; set; }

        [Required]
        [Display(Name = "To Date *")]
        [DataType(DataType.Date)]
        public string ToDate { get; set; }

        [Range(0.00,5.00,ErrorMessage ="Input a value between 0 and 5.")]
        public string Score { get; set; }
        public string Comment { get; set; }

        [Required]
        [Display(Name = "Detail *")]
        public string Detail { get; set; }

        public IEnumerable<SelectListItem> QualificationTypeSelectList
        {
            get;
            set;
        }

        public IEnumerable<SelectListItem> RelatedQualificationCodes
        {
            get;
            set;
        }
    }

    public class ApplicantEmploymentHistory
    {
        // Use internally to track the application the record is attached to.
        public string  ApplicationNo { get; set; }

        [Required]
        [Display(Name = "Institution/Company Name *")]
        public string CompanyName { get; set; }

        [Display(Name = "Job Title")]
        public string JobTitle { get; set; }

        [Required]
        [Display(Name = "From Date *")]
        [DataType(DataType.Date)]
        public string FromDate { get; set; }

        [Required]
        [Display(Name = "To Date *")]
        [DataType(DataType.Date)]
        public string ToDate { get; set; }

        public string Department { get; set; }

        public string Comment { get; set; }
    }

    // Used to pass the field index number to the template to properly name html elements in a list. ex. class[0].property
    public class HtmlFieldIndexer
    {
        public int Index { get; set; }
    }
}
