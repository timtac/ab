﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace NAVRoutine
{
    using PerformanceMgmtRef;

    public class AppraisalIndexViewModel
    {
        public List<PerformanceMgmt> Appraisals { get; set; }
    }

    public class AppraisalDetailViewModel
    {
        public PerformanceMgmt Appraisal { get; set; }
    }

    public class AppraisalViewModel
    {
        #region -----=------ AUTO POPULATED FIELDS -----------------

        public string AppraisalNo { get; set; }
        public string AppraisalPeriod { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeNo { get; set; }
        public string JobTitle { get; set; }
        public string EmployeeBranch { get; set; }
        public string DateOfJoining { get; set; }
        public string DateOfLastAppraisal { get; set; }
        public string AppraiserName { get; set; }
        public string AppraiserJobTitle { get; set; }

        // HR Performance Management respective Card (managerial/non-managerial);(which have been agreed the previous year for the current appraisal period; should be editable and adding more tasks/ objectives should be possible)
        public string TaskObjective { get; set; }

        #endregion Auto Populated fields from FRD
        // Use to track if the submission is coming from an appraisee or an appraiser
        public bool IsAppraiser { get; set; }
        // Get selectitems list for selection of ratings.
        //PerformanceRatingSelectList
        public List<SelectListItem> Rating { get; set; }

        public List<PerformanceCriteria> PerformanceCriteriaPreCurrYear { get; set; }

        public BehaviourTemplateGroup CompetencyCriteriaBehaviour { get; set; }
        public BehaviourTemplateGroup SelfAssesementIndividualDev { get; set; }

        public List<PerformanceCriteria> ObjectiveForNextAppraisal { get; set; }
        public List<PerformanceCriteria> OpenRoomDiscussion { get; set; }

        // Default constructor
        public AppraisalViewModel()
        {
            PerformanceCriteriaPreCurrYear = new List<NAVRoutine.PerformanceCriteria>();
            ObjectiveForNextAppraisal = new List<PerformanceCriteria>();
            OpenRoomDiscussion = new List<PerformanceCriteria>();
        }
    }

    public class BehaviourTemplateGroup
    {
        public string GroupName { get; set; }
        public bool UseGrading { get; set; }
        public List<SelectListItem> Rating { get; set; }
        public List<PerformanceCriteria> GroupMembers { get; set; }

        public BehaviourTemplateGroup()
        {
            GroupMembers = new List<PerformanceCriteria>();
        }
    }

    public class PerformanceCriteria
    {
        public string Objective { get; set; }
        public string Description { get; set; }

        public string SelfAssesment { get; set; }
        public string AppraiserAssessment { get; set; }

        // 
        public List<SelectListItem> ObjectiveRating { get; set; }
        public List<SelectListItem> DescriptionRating { get; set; }
    }
}
