namespace EssPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DATENUL : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE dbo.AspNetUsers SET DateOfBirth = GETDATE() WHERE DateOfBirth IS NULL");
            AlterColumn("dbo.AspNetUsers", "DateOfBirth", c => c.DateTime(nullable: false, defaultValueSql: "GETDATE()" ));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AspNetUsers", "DateOfBirth", c => c.DateTime());
        }
    }
}
