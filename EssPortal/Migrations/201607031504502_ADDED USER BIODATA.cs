namespace EssPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ADDEDUSERBIODATA : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.JobApplieds", "PositionAppliedForCode", c => c.String());
            AddColumn("dbo.AspNetUsers", "FirstName", c => c.String());
            AddColumn("dbo.AspNetUsers", "MiddleName", c => c.String());
            AddColumn("dbo.AspNetUsers", "LastName", c => c.String());
            AddColumn("dbo.AspNetUsers", "Gender", c => c.String());
            AddColumn("dbo.AspNetUsers", "DateOfBirth", c => c.DateTime(nullable: false));
            AddColumn("dbo.AspNetUsers", "Age", c => c.String());
            AddColumn("dbo.AspNetUsers", "MaritalStatus", c => c.String());
            AddColumn("dbo.AspNetUsers", "EthnicOrigin", c => c.String());
            AddColumn("dbo.AspNetUsers", "Citizenship", c => c.String());
            AddColumn("dbo.AspNetUsers", "CountryDetails", c => c.String());
            AddColumn("dbo.AspNetUsers", "HomePhoneNumber", c => c.String());
            AddColumn("dbo.AspNetUsers", "PostCode", c => c.String());
            AddColumn("dbo.AspNetUsers", "PostalAddress", c => c.String());
            AddColumn("dbo.AspNetUsers", "ResidentialAddress", c => c.String());
            AddColumn("dbo.AspNetUsers", "CellPhoneNo", c => c.String());
            AddColumn("dbo.AspNetUsers", "WorkPhoneNo", c => c.String());
            AddColumn("dbo.AspNetUsers", "FirstLanguageRWS", c => c.String());
            AddColumn("dbo.AspNetUsers", "FirstLanguageSpeak", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetUsers", "FirstLanguageRead", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetUsers", "FirstLanguageWrite", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetUsers", "SecondLanguageRWS", c => c.String());
            AddColumn("dbo.AspNetUsers", "SecondLanguageSpeak", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetUsers", "SecondLanguageRead", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetUsers", "SecondLanguageWrite", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "SecondLanguageWrite");
            DropColumn("dbo.AspNetUsers", "SecondLanguageRead");
            DropColumn("dbo.AspNetUsers", "SecondLanguageSpeak");
            DropColumn("dbo.AspNetUsers", "SecondLanguageRWS");
            DropColumn("dbo.AspNetUsers", "FirstLanguageWrite");
            DropColumn("dbo.AspNetUsers", "FirstLanguageRead");
            DropColumn("dbo.AspNetUsers", "FirstLanguageSpeak");
            DropColumn("dbo.AspNetUsers", "FirstLanguageRWS");
            DropColumn("dbo.AspNetUsers", "WorkPhoneNo");
            DropColumn("dbo.AspNetUsers", "CellPhoneNo");
            DropColumn("dbo.AspNetUsers", "ResidentialAddress");
            DropColumn("dbo.AspNetUsers", "PostalAddress");
            DropColumn("dbo.AspNetUsers", "PostCode");
            DropColumn("dbo.AspNetUsers", "HomePhoneNumber");
            DropColumn("dbo.AspNetUsers", "CountryDetails");
            DropColumn("dbo.AspNetUsers", "Citizenship");
            DropColumn("dbo.AspNetUsers", "EthnicOrigin");
            DropColumn("dbo.AspNetUsers", "MaritalStatus");
            DropColumn("dbo.AspNetUsers", "Age");
            DropColumn("dbo.AspNetUsers", "DateOfBirth");
            DropColumn("dbo.AspNetUsers", "Gender");
            DropColumn("dbo.AspNetUsers", "LastName");
            DropColumn("dbo.AspNetUsers", "MiddleName");
            DropColumn("dbo.AspNetUsers", "FirstName");
            DropColumn("dbo.JobApplieds", "PositionAppliedForCode");
        }
    }
}
