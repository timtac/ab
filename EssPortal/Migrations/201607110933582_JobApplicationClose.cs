namespace EssPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class JobApplicationClose : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.JobApplieds", "ApplicationCloseDate", c => c.DateTime());
            AddColumn("dbo.JobApplieds", "ApplicationClosed", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.JobApplieds", "ApplicationClosed");
            DropColumn("dbo.JobApplieds", "ApplicationCloseDate");
        }
    }
}
