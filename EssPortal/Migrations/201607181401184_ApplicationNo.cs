namespace EssPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ApplicationNo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.JobApplieds", "JobApplicationNo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.JobApplieds", "JobApplicationNo");
        }
    }
}
