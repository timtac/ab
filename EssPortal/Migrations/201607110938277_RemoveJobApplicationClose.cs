namespace EssPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveJobApplicationClose : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.JobApplieds", "ApplicationCloseDate");
            DropColumn("dbo.JobApplieds", "ApplicationClosed");
        }
        
        public override void Down()
        {
            AddColumn("dbo.JobApplieds", "ApplicationClosed", c => c.Boolean(nullable: false));
            AddColumn("dbo.JobApplieds", "ApplicationCloseDate", c => c.DateTime());
        }
    }
}
