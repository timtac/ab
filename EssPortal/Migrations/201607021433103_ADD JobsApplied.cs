namespace EssPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ADDJobsApplied : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.JobApplieds",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ApplicationUserId = c.String(),
                        JobNo = c.String(),
                        DateApplied = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.JobApplieds");
        }
    }
}
