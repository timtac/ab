namespace EssPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addidNo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "IdNo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "IdNo");
        }
    }
}
