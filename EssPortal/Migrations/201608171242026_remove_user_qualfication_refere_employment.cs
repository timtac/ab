namespace EssPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class remove_user_qualfication_refere_employment : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.EmploymentHistories", "ApplicationUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Qualifications", "ApplicationUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Referees", "ApplicationUserId", "dbo.AspNetUsers");
            DropIndex("dbo.EmploymentHistories", new[] { "ApplicationUserId" });
            DropIndex("dbo.Qualifications", new[] { "ApplicationUserId" });
            DropIndex("dbo.Referees", new[] { "ApplicationUserId" });
            DropTable("dbo.EmploymentHistories");
            DropTable("dbo.Qualifications");
            DropTable("dbo.Referees");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Referees",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ApplicationUserId = c.String(maxLength: 128),
                        ApplicationNo = c.String(),
                        Designation = c.String(),
                        Names = c.String(),
                        Institution = c.String(),
                        Address = c.String(),
                        TelephoneNo = c.String(),
                        EMail = c.String(),
                        CreationDate = c.DateTime(nullable: false),
                        LastModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Qualifications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ApplicationUserId = c.String(maxLength: 128),
                        QualificationType = c.String(),
                        QualificationCode = c.String(),
                        Institution = c.String(),
                        FromDate = c.DateTime(nullable: false),
                        ToDate = c.DateTime(nullable: false),
                        Score = c.String(),
                        Comment = c.String(),
                        Detail = c.String(),
                        CreationDate = c.DateTime(nullable: false),
                        LastModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EmploymentHistories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ApplicationUserId = c.String(maxLength: 128),
                        CompanyName = c.String(),
                        JobTitle = c.String(),
                        FromDate = c.DateTime(nullable: false),
                        ToDate = c.DateTime(nullable: false),
                        Department = c.String(),
                        Comment = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.Referees", "ApplicationUserId");
            CreateIndex("dbo.Qualifications", "ApplicationUserId");
            CreateIndex("dbo.EmploymentHistories", "ApplicationUserId");
            AddForeignKey("dbo.Referees", "ApplicationUserId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Qualifications", "ApplicationUserId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.EmploymentHistories", "ApplicationUserId", "dbo.AspNetUsers", "Id");
        }
    }
}
