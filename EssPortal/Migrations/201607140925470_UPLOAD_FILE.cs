namespace EssPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UPLOAD_FILE : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Attachments",
                c => new
                    {
                        AttachmentId = c.Int(nullable: false, identity: true),
                        JobAppliedId = c.Int(nullable: false),
                        FilePath = c.String(),
                        MimeType = c.String(),
                    })
                .PrimaryKey(t => t.AttachmentId)
                .ForeignKey("dbo.JobApplieds", t => t.JobAppliedId, cascadeDelete: true)
                .Index(t => t.JobAppliedId);
            
            AddColumn("dbo.AspNetUsers", "AddressStreet", c => c.String());
            AddColumn("dbo.AspNetUsers", "AddressDistrict", c => c.String());
            AddColumn("dbo.AspNetUsers", "AddressCity", c => c.String());
            AlterColumn("dbo.JobApplieds", "ApplicationUserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.JobApplieds", "ApplicationUserId");
            AddForeignKey("dbo.JobApplieds", "ApplicationUserId", "dbo.AspNetUsers", "Id");
            DropColumn("dbo.AspNetUsers", "Address");
            DropColumn("dbo.AspNetUsers", "City");
            DropColumn("dbo.AspNetUsers", "State");
            DropColumn("dbo.AspNetUsers", "CountryDetails");
            DropColumn("dbo.AspNetUsers", "ResidentialAddress");
            DropColumn("dbo.AspNetUsers", "FirstLanguageRWS");
            DropColumn("dbo.AspNetUsers", "FirstLanguageSpeak");
            DropColumn("dbo.AspNetUsers", "FirstLanguageRead");
            DropColumn("dbo.AspNetUsers", "FirstLanguageWrite");
            DropColumn("dbo.AspNetUsers", "SecondLanguageRWS");
            DropColumn("dbo.AspNetUsers", "SecondLanguageSpeak");
            DropColumn("dbo.AspNetUsers", "SecondLanguageRead");
            DropColumn("dbo.AspNetUsers", "SecondLanguageWrite");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "SecondLanguageWrite", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetUsers", "SecondLanguageRead", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetUsers", "SecondLanguageSpeak", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetUsers", "SecondLanguageRWS", c => c.String());
            AddColumn("dbo.AspNetUsers", "FirstLanguageWrite", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetUsers", "FirstLanguageRead", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetUsers", "FirstLanguageSpeak", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetUsers", "FirstLanguageRWS", c => c.String());
            AddColumn("dbo.AspNetUsers", "ResidentialAddress", c => c.String());
            AddColumn("dbo.AspNetUsers", "CountryDetails", c => c.String());
            AddColumn("dbo.AspNetUsers", "State", c => c.String());
            AddColumn("dbo.AspNetUsers", "City", c => c.String());
            AddColumn("dbo.AspNetUsers", "Address", c => c.String());
            DropForeignKey("dbo.Attachments", "JobAppliedId", "dbo.JobApplieds");
            DropForeignKey("dbo.JobApplieds", "ApplicationUserId", "dbo.AspNetUsers");
            DropIndex("dbo.JobApplieds", new[] { "ApplicationUserId" });
            DropIndex("dbo.Attachments", new[] { "JobAppliedId" });
            AlterColumn("dbo.JobApplieds", "ApplicationUserId", c => c.String());
            DropColumn("dbo.AspNetUsers", "AddressCity");
            DropColumn("dbo.AspNetUsers", "AddressDistrict");
            DropColumn("dbo.AspNetUsers", "AddressStreet");
            DropTable("dbo.Attachments");
        }
    }
}
