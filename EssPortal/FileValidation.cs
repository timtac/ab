﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Linq;
using System.Web;

namespace EssPortal
{
    public static class AcceptedContentType
    {
        public static List<string> ContentTypes { get {
                return new List<string> {
                    "image/jpeg",
                    "application/pdf",
                    "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                    "application/msword"
                };
            } }
    }
    public class ValidateFileAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var file = value as HttpPostedFileBase;
            if (file == null)
            {
                return true;
            }
            if (file.ContentLength > 3 * 1024 * 1024)
            {
                return false;
            }
            
            return AcceptedContentType.ContentTypes.Contains(file.ContentType);
        }
    }

    class RequiredValidateFileAttribute : ValidateFileAttribute
    {
        public override bool IsValid(object value)
        {
            var file = value as HttpPostedFileBase;

            if (file == null)
            {
                return false;
            }

            if (file.ContentLength > 3 * 1024 * 1024)
            {
                return false;
            }
           
            return AcceptedContentType.ContentTypes.Contains(file.ContentType);
           
        }
    }
}