﻿using EssPortal.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EssPortal.Models
{
    public class LeaveInformation
    {
        public EmployeeDataResponse EmployeeDataResponse { get; set; }
        public LeaveResponse LeavePendingApproval { get; set; }
        public bool UserIsSupervisor
        {
            //get
            //{
            //    return this.EmployeeDataResponse.Employee.Employee_Type == EmployeeRef.Employee_Type.Manager ? true : false;
            //}
            //set
            //{
            //    UserIsSupervisor = value;
            //}
            get;
            set;
        }
    }

    public class LeaveIndexViewModel : LeaveInformation
    {
        public LeaveHistoryResponse LeaveHistoryResponse { get; set; }
        public LeaveResponse LeaveResponse { get; set; }
    }
}