﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EssPortal.Models
{
    public class InvalidEmployee
    {
        public string Email { get; set; }
        public string NavUserId { get; set; }
        public string EmployeeNo { get; set; }
    }
}