﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EssPortal.Classes
{
    using VacancyRef;
    using EmployeeRef;
    using JobApplicationRef;
    using JobApplicationRefereeRef;
    using LeaveHistoryRef;
    using LeaveRef;
    using System.Web.Mvc;
    using CountryRef;
    using LookUpValuesRef;
    using JobQualificationRef;
    using JobApplicationQualificationRef;
    using MailTemplateRef;
    using System.Net;
    using JobApplicationEmpHistoryRef;
    using PerformanceMgmtRef;
    using ApplicationAttachmentRef;
    using JobRequirementRef;
    using PerformancePeriodRef;
    using PerformanceRatingsRef;
    using PerformanceBehaviourCodeRef;
    using PerformaceBehaveTemplateRef;
    using HrJobsRef;
    using RecruitmentMgmtRef;
    using Models;
    using HrSetupRef;
    using ApprovalUserSetUpRef;
    using System.Globalization;
    using PerformanceMgmtHistRef;
    using PerformanceMgmtLinesRef;

    public class NAVServiceWrapper
    {
        public string NavServer { get; set; }
        public string NavPort { get; set; }
        public string NavInstance { get; set; }
        public string NavCompany { get; set; }
        public string NavUser { get; set; }
        public string NavPwd { get; set; }
        public bool UseDefaultCredential { get; set; }
        public LogUtil LogUtil { get; set; }
        public string AppraisalCompetencyCriteriaBehaviourKey { get; set; }
        public string AppraisalSelfAssesementIndividualDevKey { get; set; }
        public string UploadFilePath { get; set; }
        public string DateFormat { get; set; }
        public string EducationQualificationTypeCode { get; set; }
        public string LanguageCode { get; set; }

        string _nullEmployee = "Employee Number was not specified";

        public string GetNavWebServiceUrl(string objectName, string serviceName)
        {
            string webServiceUrl = "http://" + this.NavServer + ":" + this.NavPort + "/" + this.NavInstance + "/WS/" + this.NavCompany + "/" + objectName + "/" + serviceName;
            return webServiceUrl.Replace(" ", "%20");
        }

        /// <summary>
        /// Using the employee card, determine if the logged in user is an employee based on the email address.
        /// This is used to set a flag 'IsEmployee' on the user profile which is used to determine if the user is an employee or not.
        /// </summary>
        /// <param name="Email">The email of the currently logged in user.</param>
        /// <returns></returns>
        public IsEmployeeResponse IsEmployee(string Email)
        {
            // Ensure that the passed in string value is not null or a white space as these will return all employees and will always be true.
            if (String.IsNullOrWhiteSpace(Email))
                return new IsEmployeeResponse { IsEmployee = false, ErrorMessage = "Email address not specified." };

            string email = Email.Trim();

            Employee_Service service = new Employee_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }

            service.Url = this.GetNavWebServiceUrl("Page", "Employee");

            List<Employee_Filter> filters = new List<Employee_Filter>();

            Employee_Filter emailFilter = new Employee_Filter();
            emailFilter.Field = Employee_Fields.Company_E_Mail;
            emailFilter.Criteria = "=" + email;

            Employee_Filter active = new Employee_Filter();
            active.Field = Employee_Fields.Status;
            active.Criteria = EmployeeRef.Status.Active.ToString();

            filters.Add(emailFilter);
            filters.Add(active);

            try
            {
                var employees = service.ReadMultiple(filters.ToArray(), null, 0);

                if (employees != null && employees.Length > 0)
                    return new IsEmployeeResponse { Success = true, IsEmployee = true, EmployeeNo = employees[0].No, NavUserId = employees[0].User_ID };
                else
                    return new IsEmployeeResponse { Success = true, IsEmployee = false, ErrorMessage = "Invalid Email specified." };
            }
            catch (Exception e)
            {
                return new IsEmployeeResponse { Success = false, IsEmployee = false, ErrorMessage = e.Message };
            }
        }

        public bool HasManagerialRole(string JobId)
        {
            if (String.IsNullOrWhiteSpace(JobId))
                return false;
            string jobId = JobId.Trim();
            HrJobs_Service service = new HrJobs_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "HrJobs");

            try
            {
                var hrJob = service.Read(jobId);
                if (hrJob != null)
                {
                    return hrJob.Managerial_Responsibility == HrJobsRef.Managerial_Responsibility.Yes ? true : false;
                }
                return false;
            }
            catch (Exception e)
            {
                LogUtil.Logger("HasManagerialRole", e.Message);
                return false;
            }
        }

        public EmployeeDataResponse GetEmployee(string NavUserId)
        {
            // Ensure that the passed in string value is not null or a white space as these will return all employees and will always be true.
            if (String.IsNullOrWhiteSpace(NavUserId))
                return new EmployeeDataResponse { Success = false, ErrorMessage = "Empty value specified for employee user id." };

            string navUserId = NavUserId.Trim();

            Employee_Service service = new Employee_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "Employee");

            List<Employee_Filter> filters = new List<Employee_Filter>();

            Employee_Filter userIdFilter = new Employee_Filter();
            userIdFilter.Field = Employee_Fields.User_ID;
            userIdFilter.Criteria = navUserId;

            Employee_Filter active = new Employee_Filter();
            active.Field = Employee_Fields.Status;
            active.Criteria = EmployeeRef.Status.Active.ToString();

            filters.Add(userIdFilter);
            filters.Add(active);

            try
            {
                var employee = service.ReadMultiple(filters.ToArray(), null, 0);
                string invalidEmployee = "Employee with userId " + navUserId + " not found.";
                return new EmployeeDataResponse
                {
                    Success = true,
                    Employee = employee.Count() <= 0 ? new Employee() : employee[0],
                    ErrorMessage = employee.Count() <= 0 ? invalidEmployee : String.Empty
                };
            }
            catch (Exception e)
            {
                LogUtil.Logger("GetEmployee", e.Message);
                return new EmployeeDataResponse { Success = false, ErrorMessage = e.Message, Employee = new Employee() };
            }
        }

        // Retrieve employee data using the employeeNo
        public EmployeeDataResponse EmployeeData(string EmployeeNo)
        {
            // Ensure that the passed in string value is not null or a white space as these will return all employees and will always be true.
            if (String.IsNullOrWhiteSpace(EmployeeNo))
                return new EmployeeDataResponse { Success = false, ErrorMessage = "Empty value specified for employee No." };

            Employee_Service service = new Employee_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "Employee");

            string employeeNo = EmployeeNo.Trim();
            try
            {
                var employee = service.Read(employeeNo);
                string invalidEmployee = "Employee with employee no " + employeeNo + " not found.";
                return new EmployeeDataResponse
                {
                    Success = true,
                    Employee = employee,
                    ErrorMessage = employee == null ? invalidEmployee : String.Empty
                };
            }
            catch (Exception e)
            {
                LogUtil.Logger("EmployeeData", e.Message);
                return new EmployeeDataResponse { Success = false, ErrorMessage = e.Message, Employee = new Employee() };
            }
        }

        // Get a list of all Employees and their names as a list of select list items for display on the form.
        public EmployeesDataResponse Employees()
        {
            Employee_Service service = new Employee_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "Employee");

            List<Employee_Filter> filters = new List<Employee_Filter>();

            Employee_Filter active = new Employee_Filter();
            active.Field = Employee_Fields.Status;
            active.Criteria = EmployeeRef.Status.Active.ToString();

            filters.Add(active);

            try
            {
                var employee = service.ReadMultiple(filters.ToArray(), null, 0);
                return new EmployeesDataResponse
                {
                    Success = true,
                    Employees = employee.ToList(),
                    ErrorMessage = employee == null ? "Employee data not loaded" : String.Empty
                };
            }
            catch (Exception e)
            {
                LogUtil.Logger("Employees", e.ToString());
                return new EmployeesDataResponse { Success = false, ErrorMessage = e.Message };
            }
        }

        public RequestResponse UpdateEmployeeData(EmployeeBiodataViewModel EmployeeBiodata)
        {
            // Ensure that the passed in employee is not null.
            if (String.IsNullOrWhiteSpace(EmployeeBiodata.EmployeeNo))
                return new RequestResponse { Success = false, ErrorMessage = "Employee not specified." };

            Employee_Service service = new Employee_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "Employee");

            try
            {
                // Get the employee and update selected fields.
                Employee employee = service.Read(EmployeeBiodata.EmployeeNo);
                employee.Residential_Add_Street_No = EmployeeBiodata.ResidentialAddressStreet;
                employee.Residential_Add_District = EmployeeBiodata.ResidentialAddressDistrict;
                employee.Residential_Add_City = EmployeeBiodata.ResidentialAddressCity;
                employee.Country_Code = EmployeeBiodata.Country;
                employee.Postal_Address = EmployeeBiodata.PostalAddress;
                employee.Cell_Phone_Number = EmployeeBiodata.CellPhoneNo;
                employee.Home_Phone_Number = EmployeeBiodata.HomePhoneNumber;
                employee.E_Mail = EmployeeBiodata.Email;// EmployeeBiodata.Email;

                service.Update(ref employee);
                return new RequestResponse
                {
                    Success = true
                };
            }
            catch (Exception e)
            {
                LogUtil.Logger("UpdateEmployeeData", e.Message);
                return new RequestResponse { Success = false, ErrorMessage = e.Message };
            }
        }

        #region ----------------------- Recruitment Specific Methods ---------------------------------

        // Return a list of vacancies based on the specified criteria.
        // An internal applicant can see Requisition_Type of Both and Internal while an External can see both and External
        public List<Vacancy> GetVacancies(bool IsInternal)
        {
            Vacancy_Service service = new Vacancy_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = GetNavWebServiceUrl("Page", "Vacancy");
            List<Vacancy> vacancies = new List<Vacancy>();

            // Add the neccesary filters based on internal on External user.
            List<Vacancy_Filter> filters = new List<Vacancy_Filter>();

            // Vacancy must be approved.
            Vacancy_Filter statusFilter = new Vacancy_Filter();
            statusFilter.Field = Vacancy_Fields.Status;
            statusFilter.Criteria = VacancyRef.Status.Published.ToString();

            //Vacancy will be selected based on the logged in user internal or external.
            Vacancy_Filter filter = new Vacancy_Filter();
            filter.Field = Vacancy_Fields.Requisition_Type;
            filter.Criteria = IsInternal ? Requisition_Type.Both.ToString() + "|" + Requisition_Type.Internal.ToString() : Requisition_Type.Both.ToString() + "|" + Requisition_Type.External.ToString(); // "=Both|Internal" : "=Both|External";

            // Vacancy closing date must not be reacehed yet.
            Vacancy_Filter closingDateFilter = new Vacancy_Filter();
            closingDateFilter.Field = Vacancy_Fields.Application_deadline;
            closingDateFilter.Criteria = ">=" + DateTime.Today.ToString("MM/dd/yyyy");

            filters.Add(statusFilter);
            filters.Add(filter);
            filters.Add(closingDateFilter);

            try
            {
                vacancies = service.ReadMultiple(filters.ToArray(), null, 0).ToList();
            }
            catch (Exception e)
            {
                this.LogUtil.Logger("GetVacancies", e.Message);
            }
            return vacancies;
        }

        public JobRequirement GetJobRequirement(string JobId)
        {
            JobRequirement_Service service = new JobRequirement_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "JobRequirement");

            JobRequirement requirements = new JobRequirement();
            try
            {
                requirements = service.Read(JobId);
                return requirements;
            }
            catch (Exception e)
            {
                LogUtil.Logger("GetJobRequirement", e.Message);
                return requirements;
            }
        }

        public string GetVacancyNo(string VacancyKey)
        {
            if (String.IsNullOrWhiteSpace(VacancyKey))
            {
                return null;
            }
            string vacancyKey = VacancyKey.Trim();

            Vacancy_Service service = new Vacancy_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "Vacancy");

            try
            {
                return service.ReadByRecId(service.GetRecIdFromKey(vacancyKey)).Requisition_No;
            }
            catch (Exception e)
            {
                LogUtil.Logger("GetVacancyNo", e.Message);
                return null;
            }
        }

        // Get a previously submitted job application for display to the user.
        public JobApplicationDetail GetJobApplication(string JobApplicationNo)
        {
            if (String.IsNullOrWhiteSpace(JobApplicationNo))
            {
                return new JobApplicationDetail { Success = false, ErrorMessage = "Job Application No not specified." };
            }
            string jobAppliationNo = JobApplicationNo.Trim();

            JobApplicationDetail model = new JobApplicationDetail();

            JobApplication_Service service = new JobApplication_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "JobApplication");

            try
            {
                model.JobApplication = service.Read(jobAppliationNo);
                model.JobApplicationQualifications = GetJobApplicationQualifications(jobAppliationNo);
                model.JobApplicationReferees = GetJobApplicationReferee(jobAppliationNo);
                model.JobApplicationEmpHistorys = GetJobApplicationEmploymentHistory(jobAppliationNo);
                model.ApplicationAttachments = GetJobApplicationAttachments(jobAppliationNo);
                model.Success = true;
            }
            catch (Exception e)
            {
                LogUtil.Logger("GetJobApplication", e.Message);
            }
            return model;
        }

        protected List<JobApplicationReferee> GetJobApplicationReferee(string JobApplicationNo)
        {
            JobApplicationReferee_Service service = new JobApplicationReferee_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "JobApplicationReferee");

            List<JobApplicationReferee_Filter> filters = new List<JobApplicationReferee_Filter>();

            JobApplicationReferee_Filter filter = new JobApplicationReferee_Filter
            {
                Field = JobApplicationReferee_Fields.Job_Application_No,
                Criteria = JobApplicationNo
            };

            filters.Add(filter);

            try
            {
                return service.ReadMultiple(filters.ToArray(), null, 0).ToList();
            }
            catch (Exception e)
            {
                // Log any application error that might arise.
                LogUtil.Logger("GetJobApplicationReferee", e.Message);
                return new List<JobApplicationReferee>();
            }
        }

        protected List<JobApplicationQualification> GetJobApplicationQualifications(string JobApplicationNo)
        {
            JobApplicationQualification_Service service = new JobApplicationQualification_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "JobApplicationQualification");

            List<JobApplicationQualification_Filter> filters = new List<JobApplicationQualification_Filter>();

            JobApplicationQualification_Filter filter = new JobApplicationQualification_Filter();
            filter.Field = JobApplicationQualification_Fields.Application_No;
            filter.Criteria = JobApplicationNo;

            filters.Add(filter);

            try
            {
                return service.ReadMultiple(filters.ToArray(), null, 0).ToList();
            }
            catch (Exception e)
            {
                // Log any application error that might arise.
                LogUtil.Logger("GetJobApplicationQualifications", e.Message);
                return new List<JobApplicationQualification>();
            }
        }

        protected List<JobApplicationEmpHistory> GetJobApplicationEmploymentHistory(string JobApplicationNo)
        {
            JobApplicationEmpHistory_Service service = new JobApplicationEmpHistory_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "JobApplicationEmpHistory");

            List<JobApplicationEmpHistory_Filter> filters = new List<JobApplicationEmpHistory_Filter>();

            JobApplicationEmpHistory_Filter filter = new JobApplicationEmpHistory_Filter();
            filter.Field = JobApplicationEmpHistory_Fields.Applicant_No;
            filter.Criteria = JobApplicationNo;

            filters.Add(filter);

            try
            {
                return service.ReadMultiple(filters.ToArray(), null, 0).ToList();
            }
            catch (Exception e)
            {
                // Log any application error that might arise.
                LogUtil.Logger("GetJobApplicationEmploymentHistory", e.Message);
                return new List<JobApplicationEmpHistory>();
            }
        }

        protected List<ApplicationAttachment> GetJobApplicationAttachments(string JobApplicationNo)
        {
            ApplicationAttachment_Service service = new ApplicationAttachment_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "ApplicationAttachment");

            List<ApplicationAttachment_Filter> filters = new List<ApplicationAttachment_Filter>();

            ApplicationAttachment_Filter filter = new ApplicationAttachment_Filter();
            filter.Field = ApplicationAttachment_Fields.Application_No;
            filter.Criteria = JobApplicationNo;

            filters.Add(filter);

            try
            {
                return service.ReadMultiple(filters.ToArray(), null, 0).ToList();
            }
            catch (Exception e)
            {
                // Log any application error that might arise.
                LogUtil.Logger("GetJobApplicationAttachments", e.Message);
                return new List<ApplicationAttachment>();
            }
        }

        public JobApplicationResponse SubmitJobApplication(ApplyViewModel application)
        {
            JobApplicationResponse response = new JobApplicationResponse();
            // Remmit all submitted aplication to the NAV server.
            var biodataResponse = this.CreateBiodata(application.ApplicantBioData);

            // Ensure the application number was successfully generated for the application.
            // Tell the user to retry if the application is not successfully generated.
            if (biodataResponse.Success)
            {
                this.AddJobApplicationReferee(application.Referees, biodataResponse.JobApplicationNo);
                this.AddJobApplicantQualifications(application.Qualifications, biodataResponse.JobApplicationNo);
                this.AddJobApplicantLanguages(application.Languages, biodataResponse.JobApplicationNo);
                this.AddJobApplicantEmploymentHistory(application.EmploymentHistory, biodataResponse.JobApplicationNo);
            }
            return biodataResponse;
        }

        public static void UpdateJobApplication() { }

        // Create the biodata of the applicant on NAV
        public JobApplicationResponse CreateBiodata(ApplicantBioData biodata)
        {
            JobApplicationResponse response = new JobApplicationResponse();

            JobApplication_Service service = new JobApplication_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "JobApplication");

            JobApplication jobApplication = new JobApplication();
            // Job applied to.
            jobApplication.Title = (EssPortal.JobApplicationRef.Title)Enum.Parse(typeof(EssPortal.JobApplicationRef.Title), biodata.UserTitle);
            jobApplication.TitleSpecified = true;
            jobApplication.Employee_Requisition_No = biodata.JobApplicationNo;

            //jobApplication.Age = biodata.Age;
            jobApplication.Employee_No = biodata.EmployeeNo;
            jobApplication.Applicant_Type = biodata.ApplicantType; // TODO USE THE IsEmployee field OF THE CURRENTLY LOGGED IN USER TO Determine this.
            jobApplication.Applicant_TypeSpecified = true;
            jobApplication.Cell_Phone_Number = biodata.CellPhoneNo;
            jobApplication.Citizenship = biodata.Citizenship;
            //jobApplication.Date_Applied = DateTime.Now;
            //jobApplication.Date_AppliedSpecified = true;

            DateTime dob = new DateTime();
            if (DateTime.TryParse(biodata.DateOfBirth, out dob))
            {
                jobApplication.Date_Of_Birth = dob;
                jobApplication.Date_Of_BirthSpecified = true;
            }
            //jobApplication.Ethnic_Origin = Ethnic_Origin.African; /// Create a dropdown list fo this option.
            //jobApplication.Ethnic_OriginSpecified = true;
            jobApplication.E_Mail = biodata.Email;
            jobApplication.First_Name = biodata.FirstName;
            if (biodata.Gender == "Not Available")
                jobApplication.Gender = JobApplicationRef.Gender._blank_;
            else
                jobApplication.Gender = (JobApplicationRef.Gender)Enum.Parse(typeof(JobApplicationRef.Gender), biodata.Gender);
            jobApplication.GenderSpecified = true;
            jobApplication.Home_Phone_Number = biodata.HomePhoneNumber;
            jobApplication.Last_Name = biodata.LastName;
            if (biodata.MaritalStatus == "Not Available")
                jobApplication.Marital_Status = JobApplicationRef.Marital_Status._blank_;
            else
                jobApplication.Marital_Status = (JobApplicationRef.Marital_Status)Enum.Parse(typeof(JobApplicationRef.Marital_Status), biodata.MaritalStatus);
            jobApplication.Marital_StatusSpecified = true;
            jobApplication.Middle_Name = biodata.MiddleName;
            jobApplication.Postal_Address = biodata.PostalAddress;
            jobApplication.Residential_Add_Street_No = biodata.ResidentialAddressStreet;
            jobApplication.Residential_Add_City = biodata.ResidentialAddressCity;
            jobApplication.Residential_Add_District = biodata.ResidentialAddressDistrict;
            jobApplication.Country_Code = biodata.Country;
            jobApplication.Work_Phone_Number = biodata.WorkPhoneNo;
            decimal basicPay = 0;
            jobApplication.Asking_Basic_Pay = Decimal.TryParse(biodata.AskingBasicPay, out basicPay) ? basicPay : basicPay;
            jobApplication.Asking_Basic_PaySpecified = true;
            jobApplication.Employee_No = biodata.EmployeeNo;
            jobApplication.ID_Number = biodata.IdNo;


            string applicationNo = String.Empty;
            try
            {
                service.Create(ref jobApplication);
                applicationNo = jobApplication.Job_Application_No;
                response.Success = true;
                response.JobApplicationNo = jobApplication.Job_Application_No;
            }
            catch (Exception e)
            {
                LogUtil.Logger("CreateBioData", e.Message);
                response.Success = false;
                response.ErrorMessage = e.Message;
            }
            return response;
        }

        // Create the referees of the applicant on the 
        public void AddJobApplicationReferee(List<ApplicantReferee> referees, string JobApplicationNo)
        {
            JobApplicationReferee_Service service = new JobApplicationReferee_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "JobApplicationReferee");

            List<JobApplicationReferee> jobApplicationReferees = new List<JobApplicationReferee>();

            // Ensure the incoming data is not null as every referee must be related to a specific application.
            if (referees != null && JobApplicationNo != null)
            {
                // Create a list of the referres to be remmited to NAV.
                foreach (var referee in referees)
                {
                    JobApplicationReferee jobAppRef = new JobApplicationReferee();
                    jobAppRef.Address = referee.Address;
                    jobAppRef.Designation = referee.Designation;
                    jobAppRef.E_Mail = referee.EMail;
                    jobAppRef.Institution = referee.Institution;
                    jobAppRef.Names = referee.Names;
                    jobAppRef.Telephone_No = referee.TelephoneNo;
                    jobAppRef.Job_Application_No = JobApplicationNo;

                    jobApplicationReferees.Add(jobAppRef);
                }

                JobApplicationReferee[] refereesArray = jobApplicationReferees.ToArray();

                try
                {
                    service.CreateMultiple(ref refereesArray);
                }
                catch (Exception e)
                {
                    // Log any application error that might arise.
                    LogUtil.Logger("AddJobApplicationReferee", e.Message);
                }
            }
        }

        public void AddJobApplicantQualifications(List<ApplicantQualification> Qualifications, string JobApplicationNo)
        {
            JobApplicationQualification_Service service = new JobApplicationQualification_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "JobApplicationQualification");

            List<JobApplicationQualification> jobApplicationQualification = new List<JobApplicationQualification>();

            // Ensure the incoming data is not null as every referee must be related to a specific application.
            if (Qualifications != null && JobApplicationNo != null)
            {
                // Create a list of the referres to be remmited to NAV.
                foreach (var qualification in Qualifications)
                {
                    JobApplicationQualification jobAppRef = new JobApplicationQualification();
                    jobAppRef.Comments = qualification.Comment;
                    //jobAppRef.Description = qualification.Detail;
                    DateTime dt = new DateTime();
                    if (true)
                    {
                        DateTime.TryParse(qualification.FromDate, out dt);
                    }
                    DateTime dateAnchor = new DateTime();
                    if (DateTime.TryParse(qualification.FromDate, out dateAnchor))
                    {
                        jobAppRef.From_Date = dateAnchor;
                        jobAppRef.From_DateSpecified = true;
                    }
                    if (DateTime.TryParse(qualification.ToDate, out dateAnchor))
                    {
                        jobAppRef.To_Date = dateAnchor;
                        jobAppRef.To_DateSpecified = true;
                    }
                    jobAppRef.Institution_Company = qualification.Institution;
                    jobAppRef.Qualification_Code = qualification.QualificationType;
                    jobAppRef.Description = qualification.QualificationCode;
                    jobAppRef.Qualification_Type = this.EducationQualificationTypeCode;
                    jobAppRef.Score_ID = qualification.Score;
                    jobAppRef.Application_No = JobApplicationNo;

                    jobApplicationQualification.Add(jobAppRef);
                }

                JobApplicationQualification[] qualificationsArray = jobApplicationQualification.ToArray();

                try
                {
                    service.CreateMultiple(ref qualificationsArray);
                }
                catch (Exception e)
                {
                    // Log any application error that might arise.
                    LogUtil.Logger("AddJobApplicantQualifications", e.Message);
                }
            }
        }

        public void AddJobApplicantLanguages(List<ApplicantLanguage> Languages, string JobApplicationNo)
        {
            JobApplicationQualification_Service service = new JobApplicationQualification_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "JobApplicationQualification");

            List<JobApplicationQualification> jobApplicationQualification = new List<JobApplicationQualification>();

            // Ensure the incoming data is not null as every referee must be related to a specific application.
            if (Languages != null && JobApplicationNo != null)
            {
                // Create a list of the referres to be remmited to NAV.
                foreach (var language in Languages)
                {
                    if (!String.IsNullOrWhiteSpace(language.Language))
                    {
                        JobApplicationQualification jobAppRef = new JobApplicationQualification();
                        jobAppRef.Qualification_Code = language.Language;
                        jobAppRef.Qualification_Type = this.LanguageCode;
                        jobAppRef.Application_No = JobApplicationNo;

                        jobApplicationQualification.Add(jobAppRef);
                    }
                }

                JobApplicationQualification[] qualificationsArray = jobApplicationQualification.ToArray();

                try
                {
                    service.CreateMultiple(ref qualificationsArray);
                }
                catch (Exception e)
                {
                    // Log any application error that might arise.
                    LogUtil.Logger("AddJobApplicantLanguages", e.Message);
                }
            }
        }

        public void AddJobApplicantEmploymentHistory(List<ApplicantEmploymentHistory> EmpHistory, string JobApplicationNo)
        {
            JobApplicationEmpHistory_Service service = new JobApplicationEmpHistory_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "JobApplicationEmpHistory");

            List<JobApplicationEmpHistory> jobApplicationEmpHist = new List<JobApplicationEmpHistory>();

            // Ensure the incoming data is not null as every referee must be related to a specific application.
            if (EmpHistory != null && JobApplicationNo != null)
            {
                // Create a list of the referres to be remmited to NAV.
                foreach (var history in EmpHistory)
                {
                    JobApplicationEmpHistory jobAppEmpHist = new JobApplicationEmpHistory();
                    jobAppEmpHist.Comments = history.Comment;
                    jobAppEmpHist.Department = history.Department;
                    DateTime dt = new DateTime();
                    if (true)
                    {
                        DateTime.TryParse(history.FromDate, out dt);
                    }
                    DateTime dateAnchor = new DateTime();
                    if (DateTime.TryParse(history.FromDate, out dateAnchor))
                    {
                        jobAppEmpHist.From_Date = dateAnchor;
                        jobAppEmpHist.From_DateSpecified = true;
                    }
                    if (DateTime.TryParse(history.ToDate, out dateAnchor))
                    {
                        jobAppEmpHist.To_Date = dateAnchor;
                        jobAppEmpHist.To_DateSpecified = true;
                    }
                    jobAppEmpHist.Company_Name = history.CompanyName;
                    jobAppEmpHist.Job_Title = history.JobTitle;

                    jobAppEmpHist.Applicant_No = JobApplicationNo;

                    jobApplicationEmpHist.Add(jobAppEmpHist);
                }

                JobApplicationEmpHistory[] qualificationsArray = jobApplicationEmpHist.ToArray();

                try
                {
                    service.CreateMultiple(ref qualificationsArray);
                }
                catch (Exception e)
                {
                    // Log any application error that might arise.
                    LogUtil.Logger("AddJobApplicantEmploymentHistory", e.Message);
                }
            }
        }

        public void AddJobApplicationAttachments(List<string> filePaths, string JobApplicationNo, List<string> SectionFileAttachedDesc)
        {
            ApplicationAttachment_Service service = new ApplicationAttachment_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "ApplicationAttachment");

            List<ApplicationAttachment> jobApplicationAttach = new List<ApplicationAttachment>();

            // Ensure the incoming data is not null as every attachment must be related to a specific application.
            if (filePaths != null && JobApplicationNo != null)
            {
                // Create a list of the attachment to be remmited to NAV.
                for (int i = 0; i < filePaths.Count; i++)
                {
                    ApplicationAttachment jobAppAttach = new ApplicationAttachment();
                    jobAppAttach.Application_No = JobApplicationNo;
                    jobAppAttach.Document_Link = filePaths[i];
                    try
                    {
                        // to capture index exceptions if it occurs. IT SHOULD NOT as the number of uploaded files should be equal the number of descriptions returned.
                        jobAppAttach.Document_Description = SectionFileAttachedDesc[i];
                    }
                    catch (Exception)
                    { }

                    jobApplicationAttach.Add(jobAppAttach);
                }

                ApplicationAttachment[] attachmentArray = jobApplicationAttach.ToArray();

                try
                {
                    service.CreateMultiple(ref attachmentArray);
                }
                catch (Exception e)
                {
                    // Log any application error that might arise.
                    LogUtil.Logger("AddJobApplicationAttachments", e.Message);
                    Console.WriteLine(e.Message);
                }
            }
        }

        public List<SelectListItem> CountriesSelectList(string currentSelection)
        {
            Country_Service service = new Country_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = GetNavWebServiceUrl("Page", "Country");

            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Select Country", Value = "" });

            try
            {
                var countryList = service.ReadMultiple(null, null, 0).ToList();
                foreach (var item in countryList)
                {
                    SelectListItem selectListItem = new SelectListItem();
                    selectListItem.Text = item.Name + " (" + item.Code + ")";
                    selectListItem.Value = item.Name;
                    selectListItem.Selected = currentSelection == item.Code;
                    items.Add(selectListItem);
                }
                return items;
            }
            catch (Exception e)
            {
                this.LogUtil.Logger("CountriesSelectList", e.Message);
                return new List<SelectListItem>();
            }
        }

        // Get select list of all qualification types that can be inputed.
        //public List<SelectListItem> QualificationTypeSelectList(string CurrentSelection)
        //{
        //    var emptySelect = new SelectListItem { Text = "Select Qualification", Value = "" };
        //    List<SelectListItem> list = new List<SelectListItem>();
        //    list.Add(emptySelect);
        //    list.AddRange(FilterHrLookUpToSelectList(CurrentSelection, "Qualification Type"));
        //    return list;
        //}

        public List<SelectListItem> QualificationTypesSelectList(string QualificationType, string CurrentSelection)
        {
            var emptySelect = new SelectListItem { Text = "Select Qualification Type ...", Value = "" };
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(emptySelect);
            list.AddRange(QualificationTypes(QualificationType, CurrentSelection));
            return list;
        }

        public List<SelectListItem> LangugeTypesSelectList(string QualificationType, string CurrentSelection)
        {
            var emptySelect = new SelectListItem { Text = "Select Language ...", Value = "" };
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(emptySelect);
            list.AddRange(LanguageTypes(QualificationType, CurrentSelection));
            return list;
        }

        public List<SelectListItem> LanguageTypes(string LanguageType, string CurrentSelection)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            // Return an empty string if no qualification code is specified.
            if (String.IsNullOrWhiteSpace(LanguageType))
                return items;

            JobQualification_Service service = new JobQualification_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = GetNavWebServiceUrl("Page", "JobQualification");

            List<JobQualification_Filter> filters = new List<JobQualification_Filter>();

            JobQualification_Filter typeFilter = new JobQualification_Filter();
            typeFilter.Field = JobQualification_Fields.Qualification_Type;
            typeFilter.Criteria = LanguageType;

            filters.Add(typeFilter);

            try
            {
                var lookUpList = service.ReadMultiple(filters.ToArray(), null, 0).ToList();

                foreach (var item in lookUpList)
                {
                    SelectListItem selectListItem = new SelectListItem();
                    selectListItem.Text = item.Description;
                    selectListItem.Value = item.Code;
                    selectListItem.Selected = CurrentSelection == item.Code;
                    items.Add(selectListItem);
                }
                return items;
            }
            catch (Exception e)
            {
                this.LogUtil.Logger("LanguageTypes", e.Message);
                return new List<SelectListItem>();
            }
        }


        public List<SelectListItem> QualificationTypes(string QualificationType, string CurrentSelection)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            // Return an empty string if no qualification code is specified.
            if (String.IsNullOrWhiteSpace(QualificationType))
                return items;

            JobQualification_Service service = new JobQualification_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = GetNavWebServiceUrl("Page", "JobQualification");

            List<JobQualification_Filter> filters = new List<JobQualification_Filter>();

            JobQualification_Filter typeFilter = new JobQualification_Filter();
            typeFilter.Field = JobQualification_Fields.Qualification_Type;
            typeFilter.Criteria = QualificationType;

            filters.Add(typeFilter);

            try
            {
                var lookUpList = service.ReadMultiple(filters.ToArray(), null, 0).ToList();

                // Group the incoming data according to the description fields.
                var descGrouping = lookUpList.GroupBy(a => a.Code);

                foreach (var item in descGrouping)
                {
                    var r = lookUpList.Where(a => a.Code == item.Key).First();
                    SelectListItem selectListItem = new SelectListItem();
                    selectListItem.Text = r.Description;
                    selectListItem.Value = r.Code;
                    selectListItem.Selected = CurrentSelection == r.Code;
                    items.Add(selectListItem);
                }
                return items;
            }
            catch (Exception e)
            {
                this.LogUtil.Logger("QualificationTypes", e.Message);
                return new List<SelectListItem>();
            }
        }

        public List<SelectListItem> RelatedQualificationCodes(string QualificationType, string QualificationCode, string CurrentSelection)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            // Return an empty string if no qualification code is specified.
            if (String.IsNullOrWhiteSpace(QualificationType) || String.IsNullOrWhiteSpace(QualificationCode))
                return items;

            JobQualification_Service service = new JobQualification_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = GetNavWebServiceUrl("Page", "JobQualification");

            List<JobQualification_Filter> filters = new List<JobQualification_Filter>();

            JobQualification_Filter typeFilter = new JobQualification_Filter();
            typeFilter.Field = JobQualification_Fields.Qualification_Type;
            typeFilter.Criteria = QualificationType;

            JobQualification_Filter descFilter = new JobQualification_Filter();
            descFilter.Field = JobQualification_Fields.Code;
            descFilter.Criteria = QualificationCode;

            filters.Add(typeFilter);
            filters.Add(descFilter);

            try
            {
                var lookUpList = service.ReadMultiple(filters.ToArray(), null, 0).ToList();
                items.Add(new SelectListItem { Text = "Select Qualification Specialization ...", Value = "" });
                foreach (var item in lookUpList)
                {
                    SelectListItem selectListItem = new SelectListItem();
                    selectListItem.Text = item.Details;
                    selectListItem.Value = item.Details;
                    selectListItem.Selected = CurrentSelection == item.Details;
                    items.Add(selectListItem);
                }
                return items;
            }
            catch (Exception e)
            {
                this.LogUtil.Logger("RelatedQualificationCodes", e.Message);
                return new List<SelectListItem>();
            }
        }

        // Get the qulification related to the qualification type selected.
        //public List<SelectListItem> RelatedQualificationCodes(string QualificationType, string CurrentSelection)
        //{
        //    List<SelectListItem> items = new List<SelectListItem>();
        //    // Return an empty string if no qualification code is specified.
        //    if (QualificationType == null)
        //        return items;

        //    JobQualification_Service service = new JobQualification_Service();
        //    if (this.UseDefaultCredential)
        //        service.UseDefaultCredentials = true;
        //    else
        //    {
        //        service.UseDefaultCredentials = false;
        //        service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
        //    }
        //    service.Url = GetNavWebServiceUrl("Page", "JobQualification");

        //    List<JobQualification_Filter> filters = new List<JobQualification_Filter>();

        //    JobQualification_Filter typeFilter = new JobQualification_Filter();
        //    typeFilter.Field = JobQualification_Fields.Qualification_Type;
        //    typeFilter.Criteria = QualificationType;

        //    filters.Add(typeFilter);

        //    try
        //    {
        //        var lookUpList = service.ReadMultiple(filters.ToArray(), null, 0).ToList();
        //        foreach (var item in lookUpList)
        //        {
        //            SelectListItem selectListItem = new SelectListItem();
        //            selectListItem.Text = item.Description;
        //            selectListItem.Value = item.Code;
        //            selectListItem.Selected = CurrentSelection == item.Code;
        //            items.Add(selectListItem);
        //        }
        //        return items;
        //    }
        //    catch (Exception e)
        //    {
        //        this.LogUtil.Logger("RelatedQualificationCodes", e.Message);
        //        return new List<SelectListItem>();
        //    }
        //}


        public List<SelectListItem> FilterHrLookUpToSelectList(string CurrentSelection, string TypeFilter)
        {
            LookUpValues_Service service = new LookUpValues_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = GetNavWebServiceUrl("Page", "LookUpValues");

            List<LookUpValues_Filter> filters = new List<LookUpValues_Filter>();

            LookUpValues_Filter typeFilter = new LookUpValues_Filter();
            typeFilter.Field = LookUpValues_Fields.Type;
            typeFilter.Criteria = TypeFilter;

            filters.Add(typeFilter);

            List<SelectListItem> items = new List<SelectListItem>();

            try
            {
                var lookUpList = service.ReadMultiple(filters.ToArray(), null, 0).ToList();
                foreach (var item in lookUpList)
                {
                    SelectListItem selectListItem = new SelectListItem();
                    selectListItem.Text = item.Description;
                    selectListItem.Value = item.Code;
                    selectListItem.Selected = CurrentSelection == item.Code;
                    items.Add(selectListItem);
                }
                return items;
            }
            catch (Exception e)
            {
                this.LogUtil.Logger("FilterHrLookUpToSelectList", e.Message);
                return new List<SelectListItem>();
            }
        }
        #endregion ----------------------------------------------------------------------------------

        // Return NAV erros that might be raised back to the calling application.

        #region -----------------Leave Specific Methods-----------------------------------------------

        public LeaveCreateResponse CreateNewLeave(string UserName, DateTime StartDate, DateTime EndDate)
        {
            RecruitmentMgmt service = new RecruitmentMgmt();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Codeunit", "RecruitmentMgmt");

            try
            {
                // get the number of valid working days for the specified period
                return new LeaveCreateResponse { NewLeaveNo = service.CreateNewLeave(UserName, StartDate, EndDate), Success = true };
            }
            catch (Exception e)
            {
                LogUtil.Logger("CreateLeaveApplication", e.Message);
                return new LeaveCreateResponse { ErrorMessage = e.Message, Success = false };
            }
        }


        public ApprovalUserSetUp GetApprovalUserSetUp(string UserId)
        {
            ApprovalUserSetUp_Service service = new ApprovalUserSetUp_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "ApprovalUserSetUp");

            try
            {
                // get the number of valid working days for the specified period
                return service.Read(UserId);
            }
            catch (Exception e)
            {
                LogUtil.Logger("GetApprovalUserSetUp", e.Message);
                return new ApprovalUserSetUp();
            }
        }

        public HrSetup GetHrSetUp()
        {
            HrSetup_Service service = new HrSetup_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "HrSetup");

            try
            {
                // get the number of valid working days for the specified period
                return service.ReadMultiple(null, null, 0).First();
            }
            catch (Exception e)
            {
                LogUtil.Logger("GetHrSetUp", e.Message);
                return new HrSetup();
            }
        }

        public decimal GetWorkingDays(DateTime startDate, int numberOfDays, bool includeHalfDay)
        {
            RecruitmentMgmt service = new RecruitmentMgmt();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Codeunit", "RecruitmentMgmt");

            try
            {
                // get the number of valid working days for the specified period
                return service.RemoveNonWorking(startDate, numberOfDays, includeHalfDay);
            }
            catch (Exception e)
            {
                LogUtil.Logger("GetWorkingDays", e.Message);
                return 0;
            }
        }

        // History should display for only the current leave period.
        public LeaveHistoryResponse GetHistoricalLeaveRecords(string EmployeeNo)
        {
            LeaveHistoryResponse response = new LeaveHistoryResponse();
            if (String.IsNullOrWhiteSpace(EmployeeNo))
            {
                response.ErrorMessage = _nullEmployee;
                return response;
            }

            LeaveHistory_Service service = new LeaveHistory_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "LeaveHistory");

            List<LeaveHistory_Filter> filters = new List<LeaveHistory_Filter>();

            LeaveHistory_Filter employeeNoFilter = new LeaveHistory_Filter();
            employeeNoFilter.Field = LeaveHistory_Fields.Employee_No;
            employeeNoFilter.Criteria = "=" + EmployeeNo;

            // TODO:  One more filter for the current leave period only.
            // Currently filtered to the current year. this might n,ot be accurate
            LeaveHistory_Filter yearFilter = new LeaveHistory_Filter();
            yearFilter.Field = LeaveHistory_Fields.Application_Date;
            yearFilter.Criteria = ">=01/01/" + DateTime.Now.Year; // Greate than the first day of the current year. i.e 1st January, of the current year.

            filters.Add(employeeNoFilter);
            filters.Add(yearFilter);

            try
            {
                response.LeaveHistory = service.ReadMultiple(filters.ToArray(), null, 0).ToList();
                response.Success = true;
                return response;
            }
            catch (Exception e)
            {
                response.ErrorMessage = e.Message;
                this.LogUtil.Logger("GetHistoricalLeaveRecords", e.Message);
                return null;
            }
        }

        // Get leave applications for the currently logged in user and display the status of the application optionally for only annual Leave. 
        public LeaveResponse GetLeaveApplications(string EmployeeNo, string AnnualLeaveCode, bool ShowOnlyAnnual = true)
        {
            LeaveResponse response = new LeaveResponse();
            if (String.IsNullOrEmpty(EmployeeNo))
            {
                response.ErrorMessage = _nullEmployee;
                return response;
            }
            Leave_Service service = new Leave_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "Leave");

            List<Leave_Filter> filters = new List<Leave_Filter>();

            Leave_Filter employeeNo = new Leave_Filter();
            employeeNo.Field = Leave_Fields.Employee_No;
            employeeNo.Criteria = String.IsNullOrWhiteSpace(EmployeeNo) ? "" : "=" + EmployeeNo;
            
            filters.Add(employeeNo);
            if (ShowOnlyAnnual)
            {
                Leave_Filter annual = new Leave_Filter
                {
                    Field = Leave_Fields.Leave_Type,
                    Criteria = AnnualLeaveCode
                };

                filters.Add(annual);
            }
                

            try
            {
                response.Leave = service.ReadMultiple("", 0, 0, 0, 0, 0, filters.ToArray(), null, 0).ToList();
                response.Success = true;
                return response;
            }
            catch (Exception e)
            {
                LogUtil.Logger("GetLeaveApplications", e.Message);
                response.ErrorMessage = e.Message;
                return response;
            }
        }

        // Get leave applications for the currently logged in user and display the status of the application.
        public LeaveResponse GetSupervisorApprovedLeaveApplications(string SupervisorEmail)
        {
            LeaveResponse response = new LeaveResponse();
            if (String.IsNullOrWhiteSpace(SupervisorEmail))
            {
                response.ErrorMessage = _nullEmployee;
                return response;
            }
            Leave_Service service = new Leave_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "Leave");

            List<Leave_Filter> filters = new List<Leave_Filter>();

            Leave_Filter supervisroEmail = new Leave_Filter();
            supervisroEmail.Field = Leave_Fields.Supervisor_Email;
            supervisroEmail.Criteria = SupervisorEmail;

            Leave_Filter approved = new Leave_Filter();
            approved.Field = Leave_Fields.Status;
            approved.Criteria = LeaveRef.Status.Approved.ToString();

            filters.Add(supervisroEmail);
            filters.Add(approved);

            try
            {
                response.Leave = service.ReadMultiple("", 0, 0, 0, 0, 0, filters.ToArray(), null, 0).ToList();
                response.Success = true;
                return response;
            }
            catch (Exception e)
            {
                LogUtil.Logger("GetLeaveApplications", e.Message);
                response.ErrorMessage = e.Message;
                return response;
            }
        }

        // Used to confirm if an employee has a running leave or a leave applied for that is new or Pending approval.
        // If true, a new leave entry will not be created for the user.
        public bool HasRunningLeave()
        {
            return false;
        }

        // Deermine if the user has any pending leave application to attend to. Display on the dashboard if so.
        public LeaveResponse PendingLeaveToApprove(string NavUserEmail)
        {
            LeaveResponse response = new LeaveResponse();
            if (String.IsNullOrEmpty(NavUserEmail))
            {
                response.ErrorMessage = "User Email was not specified.";
                return response;
            }
            Leave_Service service = new Leave_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "Leave");

            List<Leave_Filter> filters = new List<Leave_Filter>();

            Leave_Filter supervisorFilter = new Leave_Filter();
            supervisorFilter.Field = Leave_Fields.Supervisor_Email;
            supervisorFilter.Criteria = NavUserEmail;

            Leave_Filter approvalStatus = new Leave_Filter();
            approvalStatus.Field = Leave_Fields.Status;
            approvalStatus.Criteria = LeaveRef.Status.Pending_Approval.ToString();

            filters.Add(supervisorFilter);
            filters.Add(approvalStatus);

            try
            {
                response.Leave = service.ReadMultiple("", 0, 0, 0, 0, 0, filters.ToArray(), null, 0).ToList();
                response.Success = true;
                return response;
            }
            catch (Exception e)
            {
                LogUtil.Logger("PendingLeaveToApprove", e.Message);
                response.ErrorMessage = e.Message;
                return response;
            }
        }

        public DateTime StringToDate(string DateString, bool SqlDefault = false)
        {
            // Set a default date that matches with the SQL server minimum defaults.
            DateTime date = new DateTime();
            if (String.IsNullOrWhiteSpace(DateString))
            {
                if (SqlDefault)
                    return new DateTime(1900, 01, 01);
                else
                    return date;
            }
            DateTime.TryParseExact(DateString, DateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out date);
            return date;
        }

        public LeaveCreateResponse SubmitLeaveApplication(LeaveApplicationViewModel model, LeaveRef.Status LeaveStatus)
        {
            Leave_Service service = new Leave_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "Leave");

            Leave leave = new Leave();
            decimal leaveDays = 0;
            Decimal.TryParse(model.LeaveDaysAppliedFor, out leaveDays);
            //leave.Days_Applied = leaveDays;
            //leave.Days_AppliedSpecified = true;
            leave.Employee_No = model.EmployeeNo;
            leave.Leave_Type = "Annual";

            leave.Leave_Start_Date = StringToDate(model.LeaveStartDate);
            leave.Leave_Start_DateSpecified = true;

            leave.Leave_End_Date = StringToDate(model.LeaveEndDate);
            leave.Leave_End_DateSpecified = true;

            leave.Status = LeaveStatus;
            leave.StatusSpecified = true;
            if (model.RelieverCode != "." && model.RelieverCode != "")
            {
                leave.Reliever = model.RelieverCode;
            }

            LeaveCreateResponse response = new LeaveCreateResponse();
            try
            {
                service.Create("", 0, 0, 0, 0, 0, ref leave);
                response.Success = true;
            }
            catch (Exception e)
            {
                LogUtil.Logger("SubmitLeaveApplication", e.Message);
                response.ErrorMessage = e.Message;
                response.NewLeaveNo = leave.Application_Code;
            }
            return response;
        }

        public LeaveResponse GetLeaveApplication(string ApplicationCode, string EmployeeNo, string SupervisorEmail)
        {
            Leave_Service service = new Leave_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "Leave");

            LeaveResponse response = new LeaveResponse();

            List<Leave_Filter> filters = new List<Leave_Filter>();

            Leave_Filter employeeNo = new Leave_Filter();
            employeeNo.Field = Leave_Fields.Employee_No;
            employeeNo.Criteria = EmployeeNo;

            Leave_Filter applicationCode = new Leave_Filter();
            applicationCode.Field = Leave_Fields.Application_Code;
            applicationCode.Criteria = ApplicationCode;

            Leave_Filter supervisorEmail = new Leave_Filter();
            supervisorEmail.Field = Leave_Fields.Supervisor_Email;
            supervisorEmail.Criteria = SupervisorEmail;

            filters.Add(employeeNo);
            filters.Add(applicationCode);
            filters.Add(supervisorEmail);

            try
            {
                //var leave = service.Read("", 0, 0, 0, 0, 0, ApplicationCode);
                var leave = service.ReadMultiple("", 0, 0, 0, 0, 0, filters.ToArray(), null, 0);
                response.Success = true;
                response.Leave = leave.ToList();
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ErrorMessage = e.Message;
                this.LogUtil.Logger("GetLeaveApplication", e.Message);
            }
            return response;
        }

        public RequestResponse UpdateLeaveApplication(Leave Leave)
        {
            Leave_Service service = new Leave_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "Leave");

            RequestResponse response = new RequestResponse();

            try
            {
                service.Update("", 0, 0, 0, 0, 0, ref Leave);
                response.Success = true;
            }
            catch (Exception e)
            {
                response.ErrorMessage = e.Message;
                this.LogUtil.Logger("UpdateLeaveApplication", e.Message);
            }
            return response;
        }

        #endregion ----------------------------------------------------------------------------------

        #region ------------------- Appraisal Management -------------------------------------

        public string InsertAppraisal(string NavUserId)
        {
            RecruitmentMgmt service = new RecruitmentMgmt();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Codeunit", "RecruitmentMgmt");

            try
            {
                // retrieve all lines from the current appraisal period.
                return service.CreateNewRec(NavUserId);
            }
            catch (Exception e)
            {
                LogUtil.Logger("InsertAppraisal", e.Message);
                return String.Empty;
            }
        }

        public List<PerformanceRatings> PerformaceRatings()
        {
            PerformanceRatings_Service service = new PerformanceRatings_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "PerformanceRatings");

            List<PerformanceRatings> periods = new List<PerformanceRatings>();
            try
            {
                // retrieve all lines from the current appraisal period.
                periods = service.ReadMultiple(null, null, 0).ToList();
            }
            catch (Exception e)
            {
                LogUtil.Logger("PerformaceRatings", e.Message);
            }
            return periods;
        }

        public List<SelectListItem> PerformanceRatingSelectList(string currentSelection)
        {
            List<SelectListItem> selectItems = new List<SelectListItem>();
            var ratings = this.PerformaceRatings();
            selectItems.Add(new SelectListItem
            {
                Text = "Select Rating",
                Value = ""
            });

            foreach (var rating in ratings.OrderByDescending(a => a.Score))
            {
                selectItems.Add(new SelectListItem
                {
                    Text = rating.Score + " -- " + rating.Rating,
                    Value = rating.Rating,
                    Selected = currentSelection == rating.Score.ToString() ? true : false
                });
            }
            return selectItems;
        }

        public List<PerformancePeriod> ApraisalPeriod()
        {
            PerformancePeriod_Service service = new PerformancePeriod_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "PerformancePeriod");

            List<PerformancePeriod> periods = new List<PerformancePeriod>();
            try
            {
                // retrieve all lines from the current appraisal period.
                periods = service.ReadMultiple(null, null, 0).ToList();
            }
            catch (Exception e)
            {
                LogUtil.Logger("ApraisalPeriod", e.Message);
            }
            return periods;
        }

        public EmployeeAppraisalResponse GetEmployeeAppraisals(string EmployeeNo)
        {
            EmployeeAppraisalResponse response = new EmployeeAppraisalResponse();
            // Ensure an employee no is specified to avoid loading all records from NAV.
            if (String.IsNullOrWhiteSpace(EmployeeNo))
            {
                response.Success = false;
                response.ErrorMessage = "Parameter EmployeeNo is not specified.";
                return response;
            }

            PerformanceMgmt_Service service = new PerformanceMgmt_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "PerformanceMgmt");

            List<PerformanceMgmt_Filter> filters = new List<PerformanceMgmt_Filter>();

            PerformanceMgmt_Filter employeeFilter = new PerformanceMgmt_Filter();
            employeeFilter.Field = PerformanceMgmt_Fields.Employee_No;
            employeeFilter.Criteria = EmployeeNo;

            filters.Add(employeeFilter);
            try
            {
                response.Appraisals = service.ReadMultiple(filters.ToArray(), null, 0).ToList();
                response.Success = true;
                return response;
            }
            catch (Exception e)
            {
                LogUtil.Logger("GetEmployeeAppraisal", e.Message);
                response.ErrorMessage = e.Message;
                return response;
            }
        }

        public PerformanceMgmtHist EmployeePreviousYearAppraisal(string EmployeeNo, string AppraisalPeriod)
        {
            // Ensure an employee no is specified to avoid loading all records from NAV.
            if (String.IsNullOrWhiteSpace(EmployeeNo) || String.IsNullOrWhiteSpace(AppraisalPeriod))
                return null;

            PerformanceMgmtHist_Service service = new PerformanceMgmtHist_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "PerformanceMgmtHist");

            List<PerformanceMgmtHist_Filter> filters = new List<PerformanceMgmtHist_Filter>();

            PerformanceMgmtHist_Filter employeeFilter = new PerformanceMgmtHist_Filter();
            employeeFilter.Field = PerformanceMgmtHist_Fields.Employee_No;
            employeeFilter.Criteria = EmployeeNo;

            PerformanceMgmtHist_Filter period = new PerformanceMgmtHist_Filter();
            period.Field = PerformanceMgmtHist_Fields.Appraisal_Period;
            period.Criteria = AppraisalPeriod;

            filters.Add(employeeFilter);
            filters.Add(period);

            try
            {
                var appHist = service.ReadMultiple(filters.ToArray(), null, 0);
                return appHist.Length > 0 ? appHist.Last() : null;
                //return response != null ? response.Appraisal_Date.ToString(DateFormat) : "";
            }
            catch (Exception e)
            {
                LogUtil.Logger("EmployeeLastAppraisalDate", e.Message);
                return null;
            }
        }

        public string EmployeeLastAppraisalDate(string EmployeeNo, string AppraisalPeriod)
        {
            // Ensure an employee no is specified to avoid loading all records from NAV.
            if (String.IsNullOrWhiteSpace(EmployeeNo) || String.IsNullOrWhiteSpace(AppraisalPeriod))
                return "";

            PerformanceMgmt_Service service = new PerformanceMgmt_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "PerformanceMgmt");

            List<PerformanceMgmt_Filter> filters = new List<PerformanceMgmt_Filter>();

            PerformanceMgmt_Filter employeeFilter = new PerformanceMgmt_Filter();
            employeeFilter.Field = PerformanceMgmt_Fields.Employee_No;
            employeeFilter.Criteria = EmployeeNo;

            PerformanceMgmt_Filter period = new PerformanceMgmt_Filter();
            period.Field = PerformanceMgmt_Fields.Appraisal_Period;
            period.Criteria = AppraisalPeriod;

            filters.Add(employeeFilter);
            try
            {
                var response = service.ReadMultiple(filters.ToArray(), null, 0).First();
                return response != null ? response.Appraisal_Date.ToString(DateFormat) : "";
            }
            catch (Exception e)
            {
                LogUtil.Logger("EmployeeLastAppraisalDate", e.Message);
                return "";
            }
        }

        public AppraisalResponse GetAppraisal(string AppraisalNo, string AppraiseeEmpNo, string AppraiserEmpNo)
        {
            AppraisalResponse response = new AppraisalResponse();
            // Ensure an employee no is specified to avoid loading all records from NAV.
            if (String.IsNullOrWhiteSpace(AppraisalNo))
            {
                response.Success = false;
                response.ErrorMessage = "Parameter AppraisalNo is not specified.";
                return response;
            }

            PerformanceMgmt_Service service = new PerformanceMgmt_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "PerformanceMgmt");

            List<PerformanceMgmt_Filter> filters = new List<PerformanceMgmt_Filter>();

            PerformanceMgmt_Filter appraiseeEmpNo = new PerformanceMgmt_Filter();
            appraiseeEmpNo.Field = PerformanceMgmt_Fields.Employee_No;
            appraiseeEmpNo.Criteria = AppraiseeEmpNo;

            PerformanceMgmt_Filter appraisalNo = new PerformanceMgmt_Filter();
            appraisalNo.Field = PerformanceMgmt_Fields.Appraisal_No;
            appraisalNo.Criteria = AppraisalNo;

            PerformanceMgmt_Filter appraiserNo = new PerformanceMgmt_Filter();
            appraiserNo.Field = PerformanceMgmt_Fields.Supervisor_ID;
            appraiserNo.Criteria = AppraiserEmpNo;

            filters.Add(appraiseeEmpNo);
            filters.Add(appraisalNo);
            filters.Add(appraiserNo);

            try
            {
                var appraisals = service.ReadMultiple(filters.ToArray(), null, 0);
                response.Appraisal = appraisals.Length > 0 ? appraisals[0] : null;
                response.Success = true;
                return response;
            }
            catch (Exception e)
            {
                LogUtil.Logger("GetAppraisal", e.Message);
                response.ErrorMessage = e.Message;
                return response;
            }
        }

        public EmployeeAppraisalResponse GetAppraisalPendingApproval(string AppraiserEmployeeNo, bool GetSupervisorApprovedAppraisal = false)
        {
            EmployeeAppraisalResponse response = new EmployeeAppraisalResponse();
            // Ensure an employee no is specified to avoid loading all records from NAV.
            if (String.IsNullOrWhiteSpace(AppraiserEmployeeNo))
            {
                response.Success = false;
                response.ErrorMessage = "Appraiser Employee No is not specified.";
                return response;
            }

            PerformanceMgmt_Service service = new PerformanceMgmt_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "PerformanceMgmt");

            List<PerformanceMgmt_Filter> filters = new List<PerformanceMgmt_Filter>();

            PerformanceMgmt_Filter employeeFilter = new PerformanceMgmt_Filter();
            employeeFilter.Field = PerformanceMgmt_Fields.Supervisor_ID;
            employeeFilter.Criteria = AppraiserEmployeeNo;

            PerformanceMgmt_Filter statusFilter = new PerformanceMgmt_Filter();
            statusFilter.Field = PerformanceMgmt_Fields.Status;

            // Use the same logic to pull the Appraisals approved or pending approval by the Appraiser.
            if (!GetSupervisorApprovedAppraisal)
                statusFilter.Criteria = PerformanceMgmtRef.Status.Pending_Approval.ToString();
            else
                statusFilter.Criteria = PerformanceMgmtRef.Status.Approved.ToString();

            filters.Add(employeeFilter);
            filters.Add(statusFilter);

            try
            {
                response.Appraisals = service.ReadMultiple(filters.ToArray(), null, 0).ToList();
                response.Success = true;
                return response;
            }
            catch (Exception e)
            {
                LogUtil.Logger("GetEmployeeAppraisal", e.Message);
                response.ErrorMessage = e.Message;
                return response;
            }
        }

        public SubmitAppraisalResponse SubmitAppraisal(PerformanceMgmt Appraisal)
        {
            PerformanceMgmt_Service service = new PerformanceMgmt_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "PerformanceMgmt");

            try
            {
                service.Create(ref Appraisal);
                return new SubmitAppraisalResponse { Success = true, Appraisal = Appraisal };
            }
            catch (Exception e)
            {
                LogUtil.Logger("SubmitAppraisal", e.Message);
                return new SubmitAppraisalResponse { Success = false, ErrorMessage = e.Message };
            }
        }

        public void UpdateAppraisal(string AppraisalNo)
        {
            PerformanceMgmt_Service service = new PerformanceMgmt_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "PerformanceMgmt");

            PerformanceMgmt app = new PerformanceMgmt();
            // app.HRApprais[0]
            try
            {
                // Retrieve the appraisal from the server and update the neccessary fields as required.
                var appraisal = service.Read(AppraisalNo);
                // Update the neccessary fields here.

                // Commit the changes to the server.
                service.Update(ref appraisal);
            }
            catch (Exception e)
            {
                LogUtil.Logger("UpdateAppraisal", e.Message);
            }
        }

        public void UpdateAppraisal(PerformanceMgmt Appraisal)
        {
            PerformanceMgmt_Service service = new PerformanceMgmt_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "PerformanceMgmt");

            try
            {
                // Commit the changes to the server.
                service.Update(ref Appraisal);
            }
            catch (Exception e)
            {
                LogUtil.Logger("UpdateAppraisal", e.Message);
            }
        }

        public void UpdateAppraisalBehaviourTemplates(PerformanceMgmt Appraisal, List<BehaviourTemplateGroup> templates, bool IsAppraiser)
        {
            PerformanceMgmt_Service service = new PerformanceMgmt_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "PerformanceMgmt");

            foreach (var template in templates)
            {
                // Depending on the current Group chosen -Note that the group name is a key on NAV- we determine what section to load the data
                if (template.GroupName == this.AppraisalCompetencyCriteriaBehaviourKey)
                {
                    // look in the incoming data for a data field with matching Planned_Targets_Objectives fields and update.
                    var data = template.GroupMembers;
                    for (int j = 0; j < Appraisal.HR_Appraisal_Behavioural_Tab.Length; j++)
                    {
                        var match = data.Find(a => a.Objective == Appraisal.HR_Appraisal_Behavioural_Tab[j].Planned_Targets_Objectives);
                        if (match != null)
                        {
                            // Selectively update fields based on wether the request is coming from an apprasier or an Apprasiee.
                            if (IsAppraiser)
                            {
                                Appraisal.HR_Appraisal_Behavioural_Tab[j].Appraiser_Rating = match.AppraiserAssessment;
                                Appraisal.HR_Appraisal_Behavioural_Tab[j].Appraiser_Comment = match.AppraiserComment;
                            }
                            else
                            {
                                Appraisal.HR_Appraisal_Behavioural_Tab[j].Ratings = match.SelfAssesment;
                                Appraisal.HR_Appraisal_Behavioural_Tab[j].Appraisee_Comment = match.AppraiseeComment;
                            }
                        }
                    }
                }

                if (template.GroupName == this.AppraisalSelfAssesementIndividualDevKey)
                {
                    // look in the incoming data for a data field with matching Planned_Targets_Objectives fields and update.
                    var data = template.GroupMembers;
                    for (int j = 0; j < Appraisal.HR_Appraisal_General_Assessmen.Length; j++)
                    {
                        var match = data.Find(a => a.Objective == Appraisal.HR_Appraisal_General_Assessmen[j].Planned_Targets_Objectives);
                        if (match != null)
                        {
                            // Selectively update fields based on wether the request is coming from an apprasier or an Apprasiee.
                            if (IsAppraiser)
                                Appraisal.HR_Appraisal_General_Assessmen[j].Actual_Results_Manager = match.AppraiserAssessment;
                            else
                                Appraisal.HR_Appraisal_General_Assessmen[j].Appraiser_Comment = match.SelfAssesment;
                        }
                    }
                }
            }

            try
            {
                service.Update(ref Appraisal);
            }
            catch (Exception e)
            {
                LogUtil.Logger("UpdateAppraisalBehaviourTemplates", e.Message);
            }
        }

        public List<PerformanceBehaviourCode> GetBehaviouralCodes()
        {
            PerformanceBehaviourCode_Service codeService = new PerformanceBehaviourCode_Service();
            if (this.UseDefaultCredential)
            {
                codeService.UseDefaultCredentials = true;
            }
            else
            {
                codeService.UseDefaultCredentials = false;
                codeService.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            codeService.Url = this.GetNavWebServiceUrl("Page", "PerformanceBehaviourCode");
            try
            {
                var behaviourCodes = codeService.ReadMultiple(null, null, 0).ToList();
                return behaviourCodes;
            }
            catch (Exception e)
            {
                LogUtil.Logger("GetBehaviouralCodes", e.Message);
                //response.ErrorMessage = e.Message;
                //return response;
                return null;
            }
        }

        // Used to get a list of questions to be answered by the 
        public BehaviourTemplateResponse GetBehaviouralTemplate(bool HasManagerialResponsibility)
        {
            BehaviourTemplateResponse response = new BehaviourTemplateResponse();

            PerformaceBehaveTemplate_Service service = new PerformaceBehaveTemplate_Service();
            if (this.UseDefaultCredential)
            {
                service.UseDefaultCredentials = true;
            }
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "PerformaceBehaveTemplate");

            // Get the rating criteria
            var rating = this.PerformanceRatingSelectList("");
            try
            {
                // Get a list of all behavioural code and retrieve the associated behaviuoral template for each based on mangerial responsbility.
                var behaviourCodes = GetBehaviouralCodes();

                foreach (var code in behaviourCodes)
                {
                    List<PerformaceBehaveTemplate_Filter> filters = new List<PerformaceBehaveTemplate_Filter>();

                    PerformaceBehaveTemplate_Filter managerfilter = new PerformaceBehaveTemplate_Filter();
                    managerfilter.Field = PerformaceBehaveTemplate_Fields.Managerial_Responsibility;
                    string t = HasManagerialResponsibility ? PerformaceBehaveTemplateRef.Managerial_Responsibility.No.ToString() : PerformaceBehaveTemplateRef.Managerial_Responsibility.Yes.ToString();
                    t = "<>" + t;
                    managerfilter.Criteria = t;

                    PerformaceBehaveTemplate_Filter codeFilter = new PerformaceBehaveTemplate_Filter();
                    codeFilter.Field = PerformaceBehaveTemplate_Fields.Behavioural_Category;
                    codeFilter.Criteria = code.Behavioural_Group;
                    
                    filters.Add(managerfilter);
                    filters.Add(codeFilter);

                    var templates = service.ReadMultiple(filters.ToArray(), null, 0);

                    BehaviourTemplateGroup tmpl = new BehaviourTemplateGroup();
                    tmpl.GroupName = code.Behavioural_Group;

                    foreach (var item in templates)
                    {
                        tmpl.GroupMembers.Add(new PerformanceCriteria() { Objective = item.Criteria, Description = item.Description, Description_2 = item.Description_2, SelfAssesmentRating = rating, AppraiserAssessmentRating = rating });
                    }
                    response.TemplateGroup.Add(tmpl);
                }
                response.Success = true;
                return response;
            }
            catch (Exception e)
            {
                LogUtil.Logger("GetBehaviouralTemplate", e.Message);
                response.ErrorMessage = e.Message;
                return response;
            }
        }

        public RequestResponse DeleteAppraisalObjectiveAndPrevCurrYearPerCriteria(string AppraisalNo, string AppraisalPeriod)
        {
            PerformanceMgmtLines_Service service = new PerformanceMgmtLines_Service();
            if (this.UseDefaultCredential)
                service.UseDefaultCredentials = true;
            else
            {
                service.UseDefaultCredentials = false;
                service.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            service.Url = this.GetNavWebServiceUrl("Page", "PerformanceMgmtLines");

            try
            {
                List<PerformanceMgmtLines_Filter> Objfilters = new List<PerformanceMgmtLines_Filter>();

                PerformanceMgmtLines_Filter appraisalNo = new PerformanceMgmtLines_Filter();
                appraisalNo.Field = PerformanceMgmtLines_Fields.Appraisal_No;
                appraisalNo.Criteria = AppraisalNo;

                PerformanceMgmtLines_Filter appraisalPeriod = new PerformanceMgmtLines_Filter();
                appraisalPeriod.Field = PerformanceMgmtLines_Fields.Appraisal_Period;
                appraisalPeriod.Criteria = AppraisalPeriod;

                PerformanceMgmtLines_Filter KpiPeriod = new PerformanceMgmtLines_Filter();
                KpiPeriod.Field = PerformanceMgmtLines_Fields.KPI_Period;
                KpiPeriod.Criteria = KPI_Period.Next.ToString();

                Objfilters.Add(appraisalNo);
                Objfilters.Add(appraisalPeriod);
                Objfilters.Add(KpiPeriod);

                var lines = service.ReadMultiple(Objfilters.ToArray(), null, 0);
                foreach (var line in lines)
                {
                    service.Delete(line.Key);
                }

                PerformanceMgmtLines_Filter CurrKpiPeriod = new PerformanceMgmtLines_Filter();
                CurrKpiPeriod.Field = PerformanceMgmtLines_Fields.KPI_Period;
                CurrKpiPeriod.Criteria = KPI_Period.Current.ToString();

                PerformanceMgmtLines_Filter EvaluationType = new PerformanceMgmtLines_Filter();
                EvaluationType.Field = PerformanceMgmtLines_Fields.Evaluation_Type;
                EvaluationType.Criteria = Evaluation_Type.KPI.ToString();

                List<PerformanceMgmtLines_Filter> Performancefilters = new List<PerformanceMgmtLines_Filter>();
                Performancefilters.Add(appraisalNo);
                Performancefilters.Add(appraisalPeriod);
                Performancefilters.Add(CurrKpiPeriod);
                Performancefilters.Add(EvaluationType);

                var Otherlines = service.ReadMultiple(Performancefilters.ToArray(), null, 0);
                foreach (var line in Otherlines)
                {
                    service.Delete(line.Key);
                }
                return new RequestResponse { Success = true };
            }
            catch (Exception e)
            {
                LogUtil.Logger("RemoveAppraisalEntries", e.Message);
                return new RequestResponse { Success = false, ErrorMessage = e.Message };
            }
        }

        #endregion ---------------------------------------------------------------------------

        #region ------------- E MAIL --------------------------------------------------------

        public MailTemplate InitMail()
        {
            MailTemplate mailService = new MailTemplate();
            if (this.UseDefaultCredential)
                mailService.UseDefaultCredentials = true;
            else
            {
                mailService.UseDefaultCredentials = false;
                mailService.Credentials = new NetworkCredential(this.NavUser, this.NavPwd);
            }
            mailService.Url = GetNavWebServiceUrl("Codeunit", "MailTemplate");

            return mailService;
        }

        public void MailEmailConfirmation(string ConfirmationLink, string RecipientMail)
        {
            MailTemplate mailService = InitMail();
            try
            {
                mailService.EmailConfirmation(ConfirmationLink, RecipientMail);
            }
            catch (Exception e)
            {
                this.LogUtil.Logger("MailError", e.Message);
            }
        }

        public void MailAppraiserNotification(string AppraisalCode)
        {
            MailTemplate mailService = InitMail();
            try
            {
                mailService.AppraiserNotification(AppraisalCode);
            }
            catch (Exception e)
            {
                this.LogUtil.Logger("MailError", e.Message);
            }
        }

        public void MailNotifyAppraisee(string AppraisalCode)
        {
            MailTemplate mailService = InitMail();
            try
            {
                mailService.AppraiseeReviewAppraisal(AppraisalCode);
            }
            catch (Exception e)
            {
                this.LogUtil.Logger("MailError", e.Message);
            }
        }

        public void MailNotifyHrOfCompletedAppraisal(string AppraisalCode)
        {
            MailTemplate mailService = InitMail();
            try
            {
                mailService.HRAppraisalNotification(AppraisalCode);
            }
            catch (Exception e)
            {
                this.LogUtil.Logger("MailError", e.Message);
            }
        }

        public void MailApproveLeaveApp(string LeaveAppCode)
        {
            MailTemplate mailService = InitMail();
            try
            {
                mailService.ApproveLeaveApp(LeaveAppCode);
            }
            catch (Exception e)
            {
                this.LogUtil.Logger("MailError", e.Message);
            }
        }

        public void MailNotifyJobApplicants(string JobAppNo)
        {
            MailTemplate mailService = InitMail();
            try
            {
                mailService.NotifyJobApplicants(JobAppNo);
            }
            catch (Exception e)
            {
                this.LogUtil.Logger("MailError", e.Message);
            }
        }

        public void MailNotifyLeaveApprover(string LeaveAppCode)
        {
            MailTemplate mailService = InitMail();
            try
            {
                mailService.NotifyLeaveApprover(LeaveAppCode);
            }
            catch (Exception e)
            {
                this.LogUtil.Logger("MailError", e.Message);
            }
        }

        public void MailRejectLeaveApp(string LeaveAppCode)
        {
            MailTemplate mailService = InitMail();
            try
            {
                mailService.RejectLeaveApp(LeaveAppCode);
            }
            catch (Exception e)
            {
                this.LogUtil.Logger("MailError", e.Message);
            }
        }

        public void MailResetPassword(string ResetLink, string RecipientMail)
        {
            MailTemplate mailService = InitMail();
            try
            {
                mailService.ResetPassword(ResetLink, RecipientMail);
            }
            catch (Exception e)
            {
                this.LogUtil.Logger("MailError", e.Message);
            }
        }

        public void SendApproval(string LeaveAppCode)
        {
            MailTemplate mailService = InitMail();
            try
            {
                mailService.SendApproval(ref LeaveAppCode);
            }
            catch (Exception e)
            {
                this.LogUtil.Logger("MailError", e.Message);
            }
        }

        public void ApproveLeave(string LeaveAppCode, string SupervisorId)
        {
            MailTemplate mailService = InitMail();
            try
            {
                mailService.ApproveApprovalRequest(LeaveAppCode,SupervisorId);
            }
            catch (Exception e)
            {
                this.LogUtil.Logger("MailError", e.Message);
            }
        }

        public void RejectLeave(string LeaveAppCode, string SupervisorId)
        {
            MailTemplate mailService = InitMail();
            try
            {
                mailService.RejectApprovalRequest(LeaveAppCode, SupervisorId);
            }
            catch (Exception e)
            {
                this.LogUtil.Logger("MailError", e.Message);
            }
        }

        public void NotifyLeaveReliever(string LeaveAppCode)
        {
            MailTemplate mailService = InitMail();
            try
            {
                mailService.NotifyReliver(LeaveAppCode);
            }
            catch (Exception e)
            {
                this.LogUtil.Logger("MailError", e.Message);
            }
        }

        #endregion --------------------------------------------------------------------------
    }
}