﻿using EssPortal.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using EssPortal.Classes;
using System.Globalization;

namespace EssPortal
{
    public class BaseController : Controller
    {
        public ApplicationDbContext ApplicationDbContext
        {
            get
            {
                return ApplicationDbContext.Create();
            }
        }

        public string DATE_FORMAT { get { return "dd/MM/yyyy"; } }

        // Contains the code used to capture annual leave in NAV.
        public string AnnualLeaveCode
        {
            get
            {
                return ConfigurationManager.AppSettings["AnnualLeaveCode"];
            }
        }

        // Initialization to select the appropriate underlying NAV instance.
        public NAVServiceWrapper Service { get; set; }
        public LogUtil LogUtil { get; set; }
        public const string employeeRole = "Employee";
        public const string employeeGroup = "Employee";

        public void Init()
        {
            Service = new NAVServiceWrapper();
            Service.NavServer = ConfigurationManager.AppSettings["NavServer"];
            Service.NavPort = ConfigurationManager.AppSettings["NavPort"];
            Service.NavInstance = ConfigurationManager.AppSettings["NavInstance"];
            Service.NavCompany = ConfigurationManager.AppSettings["NavCompany"];
            Service.NavUser = ConfigurationManager.AppSettings["NavUser"];
            Service.NavPwd = ConfigurationManager.AppSettings["NavPwd"];
            Service.UseDefaultCredential = ConfigurationManager.AppSettings["UseDefaultCredential"] == "true" ? true : false;
            //Service.AppraisalCompetencyCriteriaBehaviourKey = ConfigurationManager.AppSettings["AppraisalCompetencyCriteriaBehaviourKey"];
            //Service.AppraisalSelfAssesementIndividualDevKey = ConfigurationManager.AppSettings["AppraisalSelfAssesementIndividualDevKey"];
            Service.UploadFilePath = ConfigurationManager.AppSettings["UploadFilePath"];
            Service.DateFormat = DATE_FORMAT;
            Service.EducationQualificationTypeCode = ConfigurationManager.AppSettings["EducationQualificationTypeCode"];
            Service.LanguageCode = ConfigurationManager.AppSettings["LanguageCode"];

            Service.LogUtil = new LogUtil();
            Service.LogUtil.FilePathLOG = ConfigurationManager.AppSettings["Log"];

            // Get the behavioural code from the server and update as
            var codes = Service.GetBehaviouralCodes();
            Service.AppraisalCompetencyCriteriaBehaviourKey = codes.Where(a => a.Behavioural_Type == PerformanceBehaviourCodeRef.Behavioural_Type.Competency).First().Behavioural_Group;

            Service.AppraisalSelfAssesementIndividualDevKey = codes.Where(a => a.Behavioural_Type == PerformanceBehaviourCodeRef.Behavioural_Type.General_Assessment).First().Behavioural_Group;

            LogUtil = new LogUtil();
            LogUtil.FilePathLOG = ConfigurationManager.AppSettings["Log"];
        }

        public InvalidEmployee InvalidEmployeeDetail()
        {
            var portalDetail = User.Identity.UserInformation();
            return new InvalidEmployee()
            {
                Email = portalDetail.Email,
                EmployeeNo = portalDetail.EmployeeNo,
                NavUserId = portalDetail.NAVUserId
            };
        }

        public bool UserIsSupervisor()
        {
            bool UserIsSupervisor = false;
            var leaveBalance = Service.EmployeeData(User.Identity.EmployeeNo());
            if (leaveBalance.Employee != null)
                UserIsSupervisor = Service.HasManagerialRole(leaveBalance.Employee.Job_ID);
            return UserIsSupervisor;
        }

        public EmployeeBiodataViewModel EmployeeBiodata()
        {
            Init();
            // Retrieve the data from the underlying NAV instance
            var employeeBiodata = Service.EmployeeData(User.Identity.EmployeeNo());
            if (employeeBiodata.Success && employeeBiodata.Employee != null)
            {
                var data = employeeBiodata.Employee;
                EmployeeBiodataViewModel userData = new EmployeeBiodataViewModel
                {
                    CellPhoneNo = data.Cell_Phone_Number,
                    //Citizenship = data.Citizenship,
                    Email = data.E_Mail,
                    FirstName = data.First_Name,
                    HomePhoneNumber = data.Home_Phone_Number,
                    LastName = data.Last_Name,
                    MiddleName = data.Middle_Name,
                    PostalAddress = data.Postal_Address,
                    EmployeeNo = data.No,
                    ResidentialAddressStreet = data.Residential_Add_Street_No,
                    ResidentialAddressDistrict = data.Residential_Add_District,
                    ResidentialAddressCity = data.Residential_Add_City,
                    Country = data.Country_Code,
                    UserTitle = data.Title.ToString()
                };
                return userData;
            }
            return new EmployeeBiodataViewModel();
        }

        // Retrieve and populate biodata with data from NAV or The portal based on wether the currently logged inuser is an employee or not.
        public ApplicantBioData UserBiodata(bool IsEmployee)
        {
            Init();
            if (IsEmployee)
            {
                // Retrieve the data from the underlying NAV instance
                var employeeBiodata = Service.EmployeeData(User.Identity.EmployeeNo());
                if (employeeBiodata.Success && employeeBiodata.Employee != null)
                {
                    var data = employeeBiodata.Employee;
                    ApplicantBioData userData = new ApplicantBioData
                    {
                        Age = data.DAge,
                        CellPhoneNo = data.Cell_Phone_Number,
                        Citizenship = data.Citizenship,
                        DateOfBirth = data.Date_Of_Birth != null ? Utils.DateDisplayString( data.Date_Of_Birth) : "",
                        Email = data.E_Mail,
                        CompanyEmail = data.Company_E_Mail,
                        FirstName = data.First_Name,
                        Gender = data.Gender == EmployeeRef.Gender._blank_ ? "Not Available" : data.Gender.ToString(),
                        HomePhoneNumber = data.Home_Phone_Number,
                        LastName = data.Last_Name,
                        MiddleName = data.Middle_Name,
                        MaritalStatus = data.Marital_Status == EmployeeRef.Marital_Status._blank_ ? "Not Available" : data.Marital_Status.ToString(),
                        PostalAddress = data.Postal_Address,
                        WorkPhoneNo = data.Work_Phone_Number,
                        ApplicantType = EssPortal.JobApplicationRef.Applicant_Type.Internal,
                        EmployeeNo = data.No,
                        ResidentialAddressStreet = data.Residential_Add_Street_No,
                        ResidentialAddressDistrict = data.Residential_Add_District,
                        ResidentialAddressCity = data.Residential_Add_City,
                        IdNo = data.ID_Number,
                        Country = data.Country_Code,
                        IsEmployee = true,
                        EmployeeJobTitle = data.Job_Title,
                        UserTitle = data.Title.ToString(),
                        PrivateMailUpdate = data.E_Mail
                    };
                    return userData;
                }
                return new ApplicantBioData();
            }
            else
            {
                var userBiodata = User.Identity.UserInformation();
                ApplicantBioData userData = new ApplicantBioData
                {
                    Age = userBiodata.Age,
                    CellPhoneNo = userBiodata.CellPhoneNo,
                    Citizenship = userBiodata.Citizenship,
                    DateOfBirth = userBiodata.DateOfBirth != null ? Utils.DateDisplayString(userBiodata.DateOfBirth) : "",
                    Email = userBiodata.Email,
                    EthnicOrigin = userBiodata.EthnicOrigin,
                    FirstName = userBiodata.FirstName,
                    LastName = userBiodata.LastName,
                    MiddleName = userBiodata.MiddleName,
                    Gender = userBiodata.Gender,
                    HomePhoneNumber = userBiodata.HomePhoneNumber,
                    MaritalStatus = userBiodata.MaritalStatus,
                    PostalAddress = userBiodata.PostalAddress,
                    WorkPhoneNo = userBiodata.WorkPhoneNo,
                    ApplicantType = EssPortal.JobApplicationRef.Applicant_Type.External,
                    ResidentialAddressStreet = userBiodata.AddressStreet,
                    ResidentialAddressDistrict = userBiodata.AddressDistrict,
                    ResidentialAddressCity = userBiodata.AddressCity,
                    IsEmployee = false,
                    UserTitle = userBiodata.Title,
                    IdNo = userBiodata.IdNo,
                    AskingBasicPay = userBiodata.AskingBasicPay.ToString(),
                    Country = userBiodata.Country,
                    PrivateMailUpdate = userBiodata.Email
                };
                return userData;
            }
        }

        // Update ApplicantBioData model with the select items required by different properties.
        public void AttachSelectListItems(ApplicantBioData biodata)
        {
            // Initialize connection to the underlying NAV instance.
            Init();
            biodata.CountriesSelectListItems = Service.CountriesSelectList(biodata.Citizenship);
        }

        // Update ApplicantBioData model with the select items required by different properties.
        public List<SelectListItem> AttachSelectListItems(string CurrentSelection)
        {
            // Initialize connection to the underlying NAV instance.
            Init();
            return Service.CountriesSelectList(CurrentSelection);
        }

        //Assign a user to a specific group  using this method. The user automatically gets all the roles assigned to the group.       
        public IdentityResult AssignUserToGroup(string UserId, string GroupName)
        {
            var groupManager = new ApplicationGroupManager();
            var group = groupManager.Groups.First(a => a.Name == GroupName);

            IdentityResult result = groupManager.SetUserGroups(UserId, new string[] { group.Id });
            return result;
        }

        public string RenderViewToString(ControllerContext context,
                                    string viewPath,
                                    object model = null,
                                    bool partial = false)
        {
            // first find the ViewEngine for this view
            ViewEngineResult viewEngineResult = null;
            if (partial)
                viewEngineResult = ViewEngines.Engines.FindPartialView(context, viewPath);
            else
                viewEngineResult = ViewEngines.Engines.FindView(context, viewPath, null);

            if (viewEngineResult == null)
                throw new FileNotFoundException("View cannot be found.");

            // get the view and attach the model to view data
            var view = viewEngineResult.View;
            context.Controller.ViewData.Model = model;

            string result = null;

            using (var sw = new StringWriter())
            {
                var ctx = new ViewContext(context, view,
                                            context.Controller.ViewData,
                                            context.Controller.TempData,
                                            sw);
                view.Render(ctx, sw);
                result = sw.ToString();
            }

            return result;
        }

        public string GetEnumDescription(Enum value)
        {
            // Get the Description attribute value for the enum value
            FieldInfo fi = value.GetType().GetField(value.ToString());
            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                    typeof(DescriptionAttribute), false);

            if (attributes.Length > 0)
            {
                return attributes[0].Description;
            }
            else
            {
                return value.ToString();
            }
        }

        // Return a select list of all employees to be used in the form as a dropdown list.
        public List<SelectListItem> EmployeeList(bool Load = false)
        {
            List<SelectListItem> employeeList = new List<SelectListItem>();
            //employeeList.Add(new SelectListItem { Text = "Select a Reliever", Value = "" }); // Add a default empty item.
            // Do not load the employee data from the code.
            if (!Load)
                return employeeList;

            this.Init();
            // Get a list of all employees in the organization.
            var employees = this.Service.Employees();
            if (employees.Success)
            {
                employees.Employees.OrderBy(a => a.No);
                foreach (var type in employees.Employees)
                {
                    SelectListItem selectListItem = new SelectListItem();
                    selectListItem.Text = type.No.ToString() + " --- " + type.First_Name + " " + type.Middle_Name + " " + type.Last_Name;
                    selectListItem.Value = type.No.ToString();
                    //selectListItem.Selected = Type == type;
                    //yield return selectListItem;
                    employeeList.Add(selectListItem);
                }
            }
            return employeeList;
        }

        // Return a select list of all employees to be used in the the select2 element.
        public List<SelectListItemsSelect2> EmployeeList2(string SelectedEmployee)
        {
            List<SelectListItemsSelect2> employeeList = new List<SelectListItemsSelect2>();
            employeeList.Add(new SelectListItemsSelect2 { text = "Select a Reliever", id = "" }); // Add a default empty item.

            this.Init();
            // Get a list of all employees in the organization.
            var employees = this.Service.Employees();
            if (employees.Success)
            {
                //employees.Employees.OrderBy(a => a.No);
                var emps = employees.Employees.OrderBy(a => a.No).Select(a => new SelectListItemsSelect2
                {
                    text = a.No.ToString() + " --- " + a.First_Name + " " + a.Middle_Name + " " + a.Last_Name,
                    id = a.No.ToString(),
                    EmployeeNo = a.No//,
                    //Selected = a.No == SelectedEmployee
                });
                employeeList.AddRange(emps);
            }
            return employeeList;
        }

        public void Success(string message, bool dismissable = false)
        {
            AddAlert(AlertStyles.Success, message, dismissable);
        }

        public void Information(string message, bool dismissable = false)
        {
            AddAlert(AlertStyles.Information, message, dismissable);
        }

        public void Warning(string message, bool dismissable = false)
        {
            AddAlert(AlertStyles.Warning, message, dismissable);
        }

        public void Danger(string message, bool dismissable = false)
        {
            AddAlert(AlertStyles.Danger, message, dismissable);
        }

        private void AddAlert(string alertStyle, string message, bool dismissable)
        {
            var alerts = TempData.ContainsKey(Alert.TempDataKey)
                ? (List<Alert>)TempData[Alert.TempDataKey]
                : new List<Alert>();

            alerts.Add(new Alert
            {
                AlertStyle = alertStyle,
                Message = message,
                Dismissable = dismissable
            });

            TempData[Alert.TempDataKey] = alerts;
        }
    }
}