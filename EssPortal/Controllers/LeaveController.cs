﻿using EssPortal.Classes;
using EssPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EssPortal.Controllers
{
    [Authorize(Roles = "Employee")]
    public class LeaveController : BaseController
    {
        // GET: Leave
        public ActionResult Index()
        {
            base.Init();
            ViewBag.IsSupervisor = base.UserIsSupervisor();
            bool UserIsSupervisor = false;
            var leaveBalance = base.Service.EmployeeData(User.Identity.EmployeeNo());
            if (leaveBalance.Employee == null)
            {
                return View("InvalidEmployeeDetail", InvalidEmployeeDetail());
            }
            UserIsSupervisor = base.Service.HasManagerialRole(leaveBalance.Employee.Job_ID);
            var currentAppliedLeave = base.Service.GetLeaveApplications(User.Identity.EmployeeNo(), AnnualLeaveCode);

            // Get a list of leave applications to be attended to by the currently logged in user.
            var leaveRequestsForApprooval = base.Service.PendingLeaveToApprove(User.Identity.UserInformation().Email);

            LeaveIndexViewModel model = new LeaveIndexViewModel { EmployeeDataResponse = leaveBalance, LeaveResponse = currentAppliedLeave, LeavePendingApproval = leaveRequestsForApprooval };//, LeaveHistoryResponse = historicalLeaveRecord };
            model.UserIsSupervisor = UserIsSupervisor;

            return View(model);
        }

        private string ValidLeaveDateFrom(DateTime EmployeeJoinDate, LeaveApplicationViewModel model)
        {
            base.Init();
            var setUp = base.Service.GetHrSetUp();
            int months = setUp.Min_Leave_App_Months;
            model.leaveMonths = months;
            return EmployeeJoinDate.AddMonths(months).ToString(DATE_FORMAT);
        }

        public ActionResult LeaveBalance()
        {
            base.Init();
            ViewBag.IsSupervisor = base.UserIsSupervisor();
            // Prepopulate the model with the information of the currently logged in user.
            LeaveApplicationViewModel model = new LeaveApplicationViewModel();
            var userdata = base.Service.EmployeeData(User.Identity.EmployeeNo());
            if (userdata.Employee == null)
                return View("InvalidEmployeeDetail", InvalidEmployeeDetail());

            var UserIsSupervisor = base.Service.HasManagerialRole(userdata.Employee.Job_ID); //userdata.Employee.Employee_Type == EssPortal.EmployeeRef.Employee_Type.Manager ? true : false;
            var leaveRequestsForApprooval = base.Service.PendingLeaveToApprove(User.Identity.UserInformation().Email);

            model.EmployeeDataResponse = userdata;
            model.LeavePendingApproval = leaveRequestsForApprooval;
            model.UserIsSupervisor = UserIsSupervisor;
            model.Employees = base.EmployeeList();

            if (userdata.Success && userdata.Employee != null)
            {
                var employeeData = userdata.Employee;
                // Get the supervisor information and attach to the model
                //var approvalEntry = base.Service.GetApprovalUserSetUp(userdata.Employee.User_ID);
                var supervisor = base.Service.EmployeeData(employeeData.Supervisor_ID).Employee;

               
                model.EmployeeNo = employeeData.No;
                //model.Department = employeeData.Department_Name;
                model.EmployeeName = employeeData.First_Name + " " + employeeData.Middle_Name + " " + employeeData.Last_Name;

                model.TotalAnnualLeaveBalance = employeeData.Leave_Balance.ToString();
                model.TotalAnnualLeaveDays = employeeData.Total_Leave_Days.ToString(); // (employeeData.Leave_Balance + employeeData.Total_Leave_Taken).ToString();
                model.TotalAnnualLeaveTaken = employeeData.Total_Leave_Taken.ToString();
                model.AnnualLeaveDaysUtilized = employeeData.Total_Leave_Taken.ToString();

                return View(model);
            }
            else
            {
                Danger(userdata.ErrorMessage, true);
                return View(model);
            }
        }

        public ActionResult CreateLeaveRequest()
        {
            base.Init();
            ViewBag.IsSupervisor = base.UserIsSupervisor();
            // Prepopulate the model with the information of the currently logged in user.
            LeaveApplicationViewModel model = new LeaveApplicationViewModel();
            var userdata = base.Service.EmployeeData(User.Identity.EmployeeNo());
            if(userdata.Employee == null)
                return View("InvalidEmployeeDetail", InvalidEmployeeDetail());

            var UserIsSupervisor = base.Service.HasManagerialRole(userdata.Employee.Job_ID); //userdata.Employee.Employee_Type == EssPortal.EmployeeRef.Employee_Type.Manager ? true : false;
            var leaveRequestsForApprooval = base.Service.PendingLeaveToApprove(User.Identity.UserInformation().Email);

            model.EmployeeDataResponse = userdata;
            model.LeavePendingApproval = leaveRequestsForApprooval;
            model.UserIsSupervisor = UserIsSupervisor;
            model.StartDateOfApprovedLeave = new DateTime().ToString();
            model.EndDateOfApprovedLeave = new DateTime().ToString();
            model.Employees = base.EmployeeList();

            if (userdata.Success && userdata.Employee != null)
            {
                // Get the supervisor information and attach to the model
                //var approvalEntry = base.Service.GetApprovalUserSetUp(userdata.Employee.User_ID);
                var supervisorData = base.Service.EmployeeData(userdata.Employee.Supervisor_ID);
                var supervisor = supervisorData.Success && supervisorData.Employee != null ? supervisorData.Employee : new EmployeeRef.Employee();

                var employeeData = userdata.Employee;
                model.EmployeeNo = employeeData.No;
                model.EmployeeName = employeeData.First_Name + " " + employeeData.Middle_Name + " " + employeeData.Last_Name;
                model.JobTitle = employeeData.Job_Title;
                model.LeaveType = "Annual";
                model.Department = employeeData.Global_Dimension_1_Code;
                model.Branch = employeeData.Global_Dimension_2_Code;
                // Ensure the default date is not a weekend
                DateTime defaultDate = DateTime.Today.DayOfWeek == DayOfWeek.Saturday ? DateTime.Today.AddDays(2)
                    : DateTime.Today.DayOfWeek == DayOfWeek.Sunday ? DateTime.Today.AddDays(1)
                    : DateTime.Today;
                model.LeaveStartDate = defaultDate.ToString(DATE_FORMAT); // SET DEFAULT DATE VALUES
                model.LeaveEndDate = defaultDate.ToString(DATE_FORMAT); // SET DEFAULT DATE VALUES
                if (supervisor != null)
                {
                    model.SupervisorEmail = supervisor.Company_E_Mail;
                    model.SupervisorName = supervisor.First_Name + " " + supervisor.Middle_Name + " " + supervisor.Last_Name;
                }
                model.TotalAnnualLeaveBalance = employeeData.Leave_Balance.ToString();
                model.TotalAnnualLeaveDays = employeeData.Total_Leave_Days.ToString();
                model.TotalAnnualLeaveTaken = employeeData.Total_Leave_Taken.ToString();
                model.ApplicationDate = DateTime.Today.ToString(DATE_FORMAT);
                model.RejectionReason = ".";
                model.LeaveDaysApproved = "0";
                model.ValidLeaveDateFrom = employeeData.End_Of_Probation_Date.ToString(DATE_FORMAT);
                return View(model);
            }
            else
            {
                Danger(userdata.ErrorMessage, true);
                return View(model);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateLeaveRequest(LeaveApplicationViewModel model)
        {
            base.Init();
            ViewBag.IsSupervisor = base.UserIsSupervisor();
            var employeeNo = User.Identity.EmployeeNo();
            var userdata = base.Service.EmployeeData(employeeNo);
            if(userdata.Employee == null)
                return View("InvalidEmployeeDetail", InvalidEmployeeDetail());

            var UserIsSupervisor = base.Service.HasManagerialRole(userdata.Employee.Job_ID); //userdata.Employee.Employee_Type == EssPortal.EmployeeRef.Employee_Type.Manager ? true : false;
            model.UserIsSupervisor = UserIsSupervisor;
            // RETURN THE MODEL IF INVALID IS DISCOVERED.
            if (!ModelState.IsValid)
            {
                model.EmployeeDataResponse = base.Service.EmployeeData(employeeNo);
                model.LeavePendingApproval = base.Service.PendingLeaveToApprove(User.Identity.UserInformation().Email);
                model.Employees = base.EmployeeList();
                return View(model);
            }

            // create the empty leave application.
            string message = "";
            LeaveCreateResponse response = new LeaveCreateResponse();
            response = base.Service.CreateNewLeave(employeeNo, Service.StringToDate(model.LeaveStartDate), Service.StringToDate(model.LeaveEndDate));
            if (response.Success)
            {
                var leaveResponse = base.Service.GetLeaveApplication(response.NewLeaveNo, employeeNo, "");

                if (leaveResponse.Success && leaveResponse.Leave[0] != null)
                {
                    // Update the application with data from the incoming form. Editable fields only.
                    var leave = leaveResponse.Leave[0];
                    if (model.SubmitLeave)
                        leave.Status = LeaveRef.Status.Pending_Approval;
                    else
                        leave.Status = LeaveRef.Status.New;
                    leave.No_of_Leave_Days_Approved = 0;
                    leave.Reliever = model.RelieverCode;
                    leave.To_Include_Half_Day = model.ToIncludeHalfDay;
                    leave.To_Include_Half_DaySpecified = true;
                    var updateResponse = base.Service.UpdateLeaveApplication(leave);

                    // Send a mail to the approver if the 
                    if (updateResponse.Success)
                    {
                        if (model.SubmitLeave)
                        {
                            message = "Your leave application has been sent to your supervisor for approval. To review your leave application and see the current status click on Leave Applications in the navigation";
                            Information(message, true);
                            Service.SendApproval(response.NewLeaveNo);
                            //base.Service.MailNotifyLeaveApprover(response.NewLeaveNo);
                        }
                        else
                        {
                            message = "Your application was saved.";
                            Information(message, true);
                        }                       
                    }
                    else
                    {
                        string str = model.SubmitLeave ? "submitted" : "updated";
                        message = "Leave application was not " + str + ". Error: " + updateResponse.ErrorMessage;
                        Danger(message, true);
                    }
                    return RedirectToAction("Index");
                }
            }

            else
            {
                Danger("Application not submitted. Error: " + response.ErrorMessage, true);
                model.EmployeeDataResponse = base.Service.EmployeeData(User.Identity.EmployeeNo());
                model.LeavePendingApproval = base.Service.PendingLeaveToApprove(User.Identity.UserInformation().Email);
                model.Employees = base.EmployeeList();
                return View(model);
            }

            Information(message, true);
            return RedirectToAction("Index");
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult CreateLeaveRequest(LeaveApplicationViewModel model)
        //{
        //    base.Init();
        //    var userdata = base.Service.EmployeeData(User.Identity.EmployeeNo());
        //    var UserIsSupervisor = base.Service.HasManagerialRole(userdata.Employee.Job_ID); //userdata.Employee.Employee_Type == EssPortal.EmployeeRef.Employee_Type.Manager ? true : false;
        //    model.UserIsSupervisor = UserIsSupervisor;
        //    // RETURN THE MODEL IF INVALID IS DISCOVERED.
        //    if (!ModelState.IsValid)
        //    {
        //        model.EmployeeDataResponse = base.Service.EmployeeData(User.Identity.EmployeeNo());
        //        model.LeavePendingApproval = base.Service.PendingLeaveToApprove(User.Identity.EmployeeNavUserId());
        //        model.Employees = base.EmployeeList();
        //        return View(model);
        //    }

        //    LeaveCreateResponse response = new LeaveCreateResponse();
        //    string message = String.Empty;
        //    // Determine if the leave is to be sent for approval or saved for subsequent editing before sending for approval.
        //    if (model.SubmitLeave)
        //    {
        //        response = base.Service.SubmitLeaveApplication(model, EssPortal.LeaveRef.Status.Pending_Approval);
        //        message = "Your leave application has been sent to your supervisor for approval.";
        //    }
        //    else
        //    {
        //        response = base.Service.SubmitLeaveApplication(model, EssPortal.LeaveRef.Status.New);
        //        message = "Your application was saved.";
        //    }
        //    if (response.Success)
        //    {
        //        if (model.SubmitLeave)
        //        {
        //            // Send success mail to applicant and notification to supervisor.
        //            base.Service.MailNotifyLeaveApprover(response.NewLeaveNo);
        //        }
        //    }
        //    else
        //    {
        //        // 
        //        Danger("Application not submitted. Error: " + response.ErrorMessage , true);
        //        model.EmployeeDataResponse = base.Service.EmployeeData(User.Identity.EmployeeNo());
        //        model.LeavePendingApproval = base.Service.PendingLeaveToApprove(User.Identity.EmployeeNavUserId());
        //        model.Employees = base.EmployeeList();
        //        return View(model);
        //    }
        //    Information(message, true);
        //    return RedirectToAction("Index");
        //}

        // Method to retrieve a leave application and populate a LeaveApplicationViewModel with it.
        private LeaveApplicationViewModel ModelFromLeave(string ApplicationCode, string EmployeeNo, string SupervisorEmail, bool LoadEmployees=false)
        {
            LeaveApplicationViewModel model = new LeaveApplicationViewModel();
            base.Init();
            // Retrieve the leave application to be updated.
            var response = base.Service.GetLeaveApplication(ApplicationCode, EmployeeNo, SupervisorEmail);
            // Display for editing using the normal SubmitLeaveRequest view.
            if (response.Success && response.Leave.Count > 0 && response.Leave[0] != null)
            {
                var leaveInformation = response.Leave[0];
                model.ApplicationNo = leaveInformation.Application_Code;
                model.EmployeeNo = leaveInformation.Employee_No;
                model.Department = leaveInformation.Department;
                model.Branch = leaveInformation.Branch;
                model.EmployeeName = leaveInformation.Names;
                model.JobTitle = leaveInformation.Job_Title;
                model.LeaveType = leaveInformation.Leave_Type;
                model.LeaveStartDate = leaveInformation.Leave_Start_Date.ToString(DATE_FORMAT);
                model.LeaveEndDate = leaveInformation.Leave_End_Date.ToString(DATE_FORMAT);
                model.LeaveDaysAppliedFor = leaveInformation.No_of_Leave_Days_Applied_for.ToString();
                model.SupervisorEmail = leaveInformation.Supervisor_Email;
                model.ToIncludeHalfDay = leaveInformation.To_Include_Half_Day;
                model.SupervisorName = leaveInformation.Supervisor;                
                model.RelieverCode = leaveInformation.Reliever;
                model.ApplicationDate = leaveInformation.Application_Date.ToString(DATE_FORMAT);
                model.LeaveApplicationStatus = leaveInformation.Status.ToString();
                model.RejectionReason = ".";
                model.LeaveDaysApproved = "0";
                model.LeaveDaysAppliedFor = leaveInformation.No_of_Leave_Days_Applied_for.ToString();
                model.Employees = base.EmployeeList(LoadEmployees);
                model.StartDateOfApprovedLeave = Utils.DateDisplayString(leaveInformation.Start_Date_of_Approved_Leave);
                model.EndDateOfApprovedLeave = Utils.DateDisplayString(leaveInformation.End_Date_of_Approved_Leave);

                var userdata = base.Service.EmployeeData(leaveInformation.Employee_No);
                model.TotalAnnualLeaveBalance = userdata.Employee.Leave_Balance.ToString();
                model.TotalAnnualLeaveDays = userdata.Employee.Total_Leave_Days.ToString();
                model.TotalAnnualLeaveTaken = userdata.Employee.Total_Leave_Taken.ToString();

                return model;
            }
            else
            {
                return null;
            }
        }

        public ActionResult Edit(string ApplicationCode)
        {
            Init();
            var employeeNo = User.Identity.EmployeeNo();
            var model = ModelFromLeave(ApplicationCode, employeeNo, "");
            model.StartDateOfApprovedLeave = new DateTime().ToString();
            model.EndDateOfApprovedLeave = new DateTime().ToString();
            var userData = base.Service.EmployeeData(employeeNo);
            ViewBag.IsSupervisor = base.UserIsSupervisor();
            if (userData.Employee == null)
                return View("InvalidEmployeeDetail", InvalidEmployeeDetail());

            if (model != null)
            {
                model.EmployeeDataResponse = userData;
                model.LeavePendingApproval = base.Service.PendingLeaveToApprove(User.Identity.UserInformation().Email);
                return View(model);
            }

            Danger("Requested leave informaton could not be retrieved.", true);
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(LeaveApplicationViewModel model)
        {
            base.Init();
            var employeeNo = User.Identity.EmployeeNo();
            var userData = base.Service.EmployeeData(employeeNo);
            if (userData.Employee == null)
                return View("InvalidEmployeeDetail", InvalidEmployeeDetail());

            ViewBag.IsSupervisor = base.UserIsSupervisor();
            model.Employees = base.EmployeeList();
            model.EmployeeDataResponse = userData;
            
            model.LeavePendingApproval = base.Service.PendingLeaveToApprove(User.Identity.UserInformation().Email);

            // RETURN THE MODEL IF INVALID IS DISCOVERED.
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            RequestResponse response = new RequestResponse();
            string message = String.Empty;
            // Determine if the leave is to be sent for approval or saved for subsequent editing before sending for approval.

            // Retrieve the leave from the server and modify only editable fields to avoid overwriting the entire record.
            var leaveResponse = base.Service.GetLeaveApplication(model.ApplicationNo, employeeNo, "");

            if (leaveResponse.Success && leaveResponse.Leave[0] != null)
            {
                // Update the application with data from the incoming form. Editable fields only.
                var leave = leaveResponse.Leave[0];

                leave.Leave_Start_Date = Service.StringToDate(model.LeaveStartDate);
                leave.Leave_Start_DateSpecified = true;

                leave.Leave_End_Date = Service.StringToDate(model.LeaveEndDate);
                leave.Leave_End_DateSpecified = true;

                leave.To_Include_Half_Day = model.ToIncludeHalfDay;
                leave.To_Include_Half_DaySpecified = true;

                //leave.Days_Applied = Decimal.Parse(model.LeaveDaysAppliedFor);
                if (model.RelieverCode != "." && model.RelieverCode != "")
                {
                    leave.Reliever = model.RelieverCode;
                }

                if (model.SubmitLeave)
                {
                    leave.Status = EssPortal.LeaveRef.Status.Pending_Approval;
                    response = base.Service.UpdateLeaveApplication(leave);
                    if (response.Success)
                    {
                        // Send success mail to applicant and notification to supervisor.
                        Service.SendApproval(leave.Application_Code);
                        //base.Service.MailNotifyLeaveApprover(leave.Application_Code);
                    }
                    message = message = "Your leave application has been sent to your supervisor for approval. To review your leave application and see the current status click on Leave Applications in the navigation";
                }
                else
                {
                    response = base.Service.UpdateLeaveApplication(leave);
                    message = "Your application was saved.";
                }
                if (response.Success)
                {
                    if (model.SubmitLeave)
                    {
                        // Send success mail to applicant and notification to supervisor.
                    }
                }
                else
                {
                    // 
                    Danger("Leave application not updated. Error: " + response.ErrorMessage, true);
                    return View(model);
                }
                Information(message, true);
                return RedirectToAction("Index");
            }
            else
            {
                Danger("Application could not be retrieved" + leaveResponse.ErrorMessage, true);
                return View(model);
            }
        }

        public ActionResult SupervisorEdit(string ApplicationCode)
        {
            base.Init();
            ViewBag.IsSupervisor = base.UserIsSupervisor();
            var employeeNo = User.Identity.EmployeeNo();
            var userData = base.Service.EmployeeData(employeeNo);
            var userEmail = User.Identity.UserInformation().Email;
            if (userData.Employee == null)
                return View("InvalidEmployeeDetail", InvalidEmployeeDetail());
            var model = ModelFromLeave(ApplicationCode, "", userEmail);
            
            if (model != null)
            {
                List<SelectListItem> employeeList = new List<SelectListItem>();
                model.Employees = employeeList;
                model.IsSupervisor = true;
                model.RejectionReason = "";
                model.LeaveDaysApproved = "";
                model.EmployeeDataResponse = userData;
                model.LeavePendingApproval = base.Service.PendingLeaveToApprove(userEmail);
                return View(model);
            }
            Danger("Requested leave informaton could not be retrieved.", true);
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SupervisorEdit(LeaveApplicationViewModel model)
        {
            base.Init();
            ViewBag.IsSupervisor = base.UserIsSupervisor();
            var employeeNo = User.Identity.EmployeeNo();
            var userData = base.Service.EmployeeData(employeeNo);
            if(userData.Employee == null)
                return View("InvalidEmployeeDetail", InvalidEmployeeDetail());

            model.EmployeeDataResponse = userData;
            var userEmail = User.Identity.UserInformation().Email;
            model.LeavePendingApproval = base.Service.PendingLeaveToApprove(userEmail);
            // RETURN THE MODEL IF INVALID IS DISCOVERED.
            if (!ModelState.IsValid)
            {
                model.Employees = base.EmployeeList();
                model.IsSupervisor = true;
                return View(model);
            }

            RequestResponse response = new RequestResponse();
            string message = String.Empty;
            // Determine if the leave is to be sent for approval or saved for subsequent editing before sending for approval.

            // Retrieve the leave from the server and modify only editable fields to avoid overwriting the entire record.
            var leaveResponse = base.Service.GetLeaveApplication(model.ApplicationNo, "", userEmail);

            if (leaveResponse.Success && leaveResponse.Leave[0] != null)
            {
                // Update the application with data from the incoming form. Editable fields only.
                var leave = leaveResponse.Leave[0];


                leave.Leave_Start_Date = Service.StringToDate(model.LeaveStartDate);
                leave.Leave_Start_DateSpecified = true;

                leave.Leave_End_Date = Service.StringToDate(model.LeaveEndDate);
                leave.Leave_End_DateSpecified = true;

                if (model.RelieverCode != "." && model.RelieverCode != "")
                {
                    leave.Reliever = model.RelieverCode;
                }

                if (model.Approved)
                {
                    // if the approved leave days are different, save the approved days then save the new dates
                    if (model.LeaveDaysAppliedFor == model.LeaveDaysApproved)
                    {
                        leave.No_of_Leave_Days_Approved = Decimal.Parse(model.LeaveDaysApproved);
                        leave.No_of_Leave_Days_ApprovedSpecified = true;
                        leave.Status = EssPortal.LeaveRef.Status.Approved;
                        response = base.Service.UpdateLeaveApplication(leave);

                    }
                    else
                    {
                        leave.No_of_Leave_Days_Approved = Decimal.Parse(model.LeaveDaysApproved);
                        leave.No_of_Leave_Days_ApprovedSpecified = true;
                        response = base.Service.UpdateLeaveApplication(leave); // this is to ensure that the approved date fields are now editable.

                        // retrieve the update leave application
                        var updatedLeave = base.Service.GetLeaveApplication(model.ApplicationNo, "", userEmail);
                        if (updatedLeave.Success && updatedLeave.Leave[0] != null)
                        {
                            var l = updatedLeave.Leave[0];
                            l.Start_Date_of_Approved_Leave = Service.StringToDate(model.StartDateOfApprovedLeave);
                            l.Start_Date_of_Approved_LeaveSpecified = true;
                            l.End_Date_of_Approved_Leave = Service.StringToDate(model.EndDateOfApprovedLeave);
                            l.End_Date_of_Approved_LeaveSpecified = true;
                            l.Status = EssPortal.LeaveRef.Status.Approved;
                            response = base.Service.UpdateLeaveApplication(l);
                        }                        
                    }                    
                }
                else
                {
                    leave.Rejection_Reason = model.RejectionReason;
                    leave.Status = EssPortal.LeaveRef.Status.Rejected;
                    response = base.Service.UpdateLeaveApplication(leave);
                }
                if (response.Success)
                {
                    if (model.Approved)
                    {
                        // Send approval mail to employee
                        message = "Leave " + leave.Application_Code + " by " + leave.Names + " has been approved.";
                        base.Service.ApproveLeave(leave.Application_Code, User.Identity.EmployeeNavUserId());
                        base.Service.MailApproveLeaveApp(leave.Application_Code);
                        base.Service.NotifyLeaveReliever(leave.Application_Code);
                    }
                    else
                    {
                        message = "Leave " + leave.Application_Code + " by " + leave.Names + " has been rejected.";
                        // Send rejection mail to employee
                        base.Service.RejectLeave(leave.Application_Code, User.Identity.EmployeeNavUserId());
                        base.Service.MailRejectLeaveApp(leave.Application_Code);
                    }
                    Information(message, true);
                    return RedirectToAction("Index", "Manage");
                }
                else
                {
                    Danger("Your request could not be completed. ERROR: " + response.ErrorMessage, true);
                    model.Employees = base.EmployeeList();
                    model.IsSupervisor = true;
                    return View(model);
                }
            }
            else
            {
                Danger("Application could not be retireved" + leaveResponse.ErrorMessage, true);
                model.Employees = base.EmployeeList();
                model.IsSupervisor = true;
                return View(model);
            }
        }

        public ActionResult SupervisorApprovedLeave()
        {
            base.Init();
            ViewBag.IsSupervisor = base.UserIsSupervisor();
            bool UserIsSupervisor = false;
            var employeeInfo = base.Service.EmployeeData(User.Identity.EmployeeNo());
            if (employeeInfo.Employee == null)
            {
                return View("InvalidEmployeeDetail", InvalidEmployeeDetail());
            }
            UserIsSupervisor = base.Service.HasManagerialRole(employeeInfo.Employee.Job_ID);
            var approvedLeave = base.Service.GetSupervisorApprovedLeaveApplications(employeeInfo.Employee.Company_E_Mail);

            // Get a list of leave applications to be attended to by the currently logged in user.
            var leaveRequestsForApprooval = base.Service.PendingLeaveToApprove(User.Identity.UserInformation().Email);

            LeaveIndexViewModel model = new LeaveIndexViewModel { EmployeeDataResponse = employeeInfo, LeaveResponse = approvedLeave, LeavePendingApproval = leaveRequestsForApprooval };//, LeaveHistoryResponse = historicalLeaveRecord };
            model.UserIsSupervisor = UserIsSupervisor;
            return View(model);
        }

        public JsonResult LeaveDaysApplied(string StartDate, string EndDate, bool includeHalfDay)
        {
            DateTime startDate = new DateTime();
            DateTime endDate = new DateTime();
            decimal daysDiff = 0;
            if (DateTime.TryParse(StartDate, out startDate) && DateTime.TryParse(EndDate, out endDate))
            {
                base.Init();
                var difference = endDate - startDate;
                int days = difference.Days;
                daysDiff = base.Service.GetWorkingDays(startDate, days, includeHalfDay);
            }

            //
            //base.Service.GetWorkingDays();
            if (includeHalfDay)
                daysDiff = daysDiff + (decimal)0.5;
            else
                daysDiff = daysDiff + 1;

            return Json(new { difference = daysDiff }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetEmployeeList(string SelectedEmployeeNo)
        {
            // Check if the select list of employees is already present in the session store and use if it is otherwise store it in the session
            List<SelectListItemsSelect2> data = new List<SelectListItemsSelect2>();
            if (Session["AllEmployeeList"] != null)
            {
                data = (List<SelectListItemsSelect2>)Session["AllEmployeeList"];
            }
            else
            {
                base.Init();
                data = base.EmployeeList2(SelectedEmployeeNo);
                Session["AllEmployeeList"] = data;
            }

            foreach (var item in data.Where(a => a.EmployeeNo == SelectedEmployeeNo))
            {
                item.Selected = true;
            }
            
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;

            var output = new ContentResult {
                Content = serializer.Serialize(data),
                ContentType = "application/json"
            };
            return output;
            //return Json(new { employeesResponse = response }, JsonRequestBehavior.AllowGet);
        }
    }
}