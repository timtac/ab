﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EssPortal.Models;
using System.IO;
using EssPortal.VacancyRef;
using EssPortal.Classes;

namespace EssPortal.Controllers
{
    [RoutePrefix("careers/recruitment")]
    [Authorize]
    public class RecruitmentController : BaseController
    {
        // GET: Recruitment
        [AllowAnonymous]
        [Route("vacancies")]
        public ActionResult Index()
        {
            RecruitmentIndexViewModel model = new RecruitmentIndexViewModel();
            // Initialize connection to the underlying NAV instance.
            Init();
            ViewBag.IsSupervisor = base.UserIsSupervisor();
            List<Vacancy> vacancies = new List<Vacancy>();
            // Determine if the currently logged in user is an Employee and display the appropriate Vacancy listing.
            if (Request.IsAuthenticated && User.IsInRole(employeeRole))
                vacancies = Service.GetVacancies(true);
            else
                vacancies = Service.GetVacancies(false);
            model.JobsAppliedTo = User.Identity.JobsAppliedFor();
            model.Vacancies = vacancies;
            return View(model);
        }

        public void AttachSelectListItems(ApplicantQualification Qualifications)
        {
            // Initialize connection to the underlying NAV instance.
            base.Init();
            Qualifications.QualificationTypeSelectList = base.Service.QualificationTypesSelectList(Service.EducationQualificationTypeCode, Qualifications.QualificationType);
            Qualifications.RelatedQualificationCodes = base.Service.RelatedQualificationCodes(Service.EducationQualificationTypeCode, Qualifications.QualificationType, Qualifications.QualificationCode);
        }

        public void AttachLanguageSelectList(ApplicantLanguage language)
        {
            base.Init();
            language.LanguagesSelectList = base.Service.LangugeTypesSelectList(Service.LanguageCode, language.Language);
        }

        public bool IsValidFile(HttpPostedFileBase file)
        {
            List<string> AcceptedContentType = new List<string> {
                "image/jpeg",
                "application/pdf",
                "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                "application/msword"
            };

            if (file.ContentLength > 3 * 1024 * 1024)
            {
                return false;
            }

            return AcceptedContentType.Contains(file.ContentType);
        }

        // Set up the location to which uploaded recruitment documents will go.
        private string FileSavePath(string ServerPath, string SubPath, string FileName, LogUtil logUtil)
        {
            string saveLocation = ServerPath.EndsWith(@"\") ? ServerPath + SubPath : ServerPath + @"\" + SubPath;
            try
            {
                if (!Directory.Exists(saveLocation))
                {
                    Directory.CreateDirectory(saveLocation);
                }
            }
            catch (Exception e)
            {
                logUtil.Logger("DirectoryCreation", e.Message);
            }
            return saveLocation + @"\" + FileName;
        }

        [Route("apply")]
        public ActionResult Apply2(string AppRef, string Position)
        {
            base.Init();
            ViewBag.IsSupervisor = base.UserIsSupervisor();
            ///ViewBag.LanguageSelectList = base.Service.LangugeTypesSelectList(Service.LanguageCode, "");
            ApplyViewModel model = new ApplyViewModel();
            // Confirm if the user has applied to the vacancy before and redirect to the profile of the user from where they can 
            //view or edit the application depending on wether or not it is closed.
            string vacNo = Service.GetVacancyNo(AppRef);
            if (User.Identity.JobAlreadyAppliedFor(vacNo) > 0)
            {
                Information("You have already applied for this vacancy.", true);
                return RedirectToAction("Index", "Recruitment");
            }
            else if (String.IsNullOrWhiteSpace(vacNo))
            {
                // Retrieve the selected application from the server
                Information("The selected opening does not exist. Please select a valid job opening.", true);
                return RedirectToAction("Index", "Recruitment");
            }
            else
            {
                // Retrieve logge in user information and prepopulate the model with the logged in user information.
                bool isEmployee = User.Identity.UserIsEmployee();
                model.ApplicantBioData = UserBiodata(isEmployee);
                model.ApplicantBioData.Email = User.Identity.Name;
                model.JobApplicationNo = vacNo;
                model.JobPosition = Position;
                model.FileAttached = new List<bool> { false, false, false };

                AttachSelectListItems(model.ApplicantBioData);

                User.Identity.GetRecruitmentDetail(model, Service, isEmployee);

                foreach (var item in model.Qualifications)
                {
                    AttachSelectListItems(item);
                }
                foreach (var item in model.Languages)
                {
                    AttachLanguageSelectList(item);
                }
            }
            return View(model);
        }

        private ApplyViewModel RemoveDeleted(ApplyViewModel model)
        {
            for (int i = 0; i < model.Referees.Count; i++)
            {
                if (model.Referees[i].IsDeleted)
                    model.Referees.Remove(model.Referees[i]);
            }
            for (int i = 0; i < model.Qualifications.Count; i++)
            {
                if(model.Qualifications[i].IsDeleted)
                    model.Qualifications.Remove(model.Qualifications[i]);
            }
            for (int i = 0; i < model.EmploymentHistory.Count; i++)
            {
                if (model.EmploymentHistory[i].IsDeleted)
                    model.EmploymentHistory.Remove(model.EmploymentHistory[i]);
            }
            return model;
        }

        [Route("apply")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Apply2(ApplyViewModel model, string AppRef, IEnumerable<HttpPostedFileBase> files)
        {
            base.Init();
            ViewBag.IsSupervisor = base.UserIsSupervisor();
            if (User.Identity.JobAlreadyAppliedFor(model.JobApplicationNo) > 0)
            {
                Information("You have already applied for this vacancy.", true);
                return RedirectToAction("Index", "Recruitment");
            }

            if (ModelState.IsValid)
            {
                // Attach an applicant type based on the currently logged in user
                if (User.Identity.UserIsEmployee())
                {
                    model.ApplicantBioData.ApplicantType = EssPortal.JobApplicationRef.Applicant_Type.Internal;
                    model.ApplicantBioData.EmployeeNo = User.Identity.EmployeeNo();
                }
                else
                {
                    // Update the applicants biodata
                    User.Identity.UpdateUserProfile(model.ApplicantBioData);

                    model.ApplicantBioData.ApplicantType = EssPortal.JobApplicationRef.Applicant_Type.External;
                }

                // CONFIRM THAT THE POSTED FILES ARE VALID
                if (Request.Files.Count > 0)
                {
                    foreach (var file in files)
                    {
                        if (file != null && file.ContentLength > 0)
                        {
                            if (!IsValidFile(file))
                            {
                                ModelState.AddModelError(String.Empty, "Upload file is not in the correct format or is larger than 3MB.");
                                AttachSelectListItems(model.ApplicantBioData);
                                foreach (var item in model.Qualifications)
                                {
                                    AttachSelectListItems(item);
                                }
                                foreach (var item in model.Languages)
                                {
                                    AttachLanguageSelectList(item);
                                }
                                return View(model);
                            }
                        }
                    }
                }

                List<string> attachments = new List<string>();
                model.ApplicantBioData.JobApplicationNo = model.JobApplicationNo;
                //string jobApplicationNo = Service.SubmitJobApplication(model);
                var appCreationResponse = Service.SubmitJobApplication(model);
                // Ensure a valid Job number was returned.
                if (appCreationResponse.Success)
                {
                    // Attach the application to the user and get the key of the new record.
                    int JobAppliedId = User.Identity.AddJobsAppliedFor(model.JobApplicationNo, model.JobPosition, appCreationResponse.JobApplicationNo);

                    // PROCESS AND SAVE EACH FILE.
                    var context = base.ApplicationDbContext;
                    if (Request.Files.Count > 0)
                    {
                        foreach (var file in files)
                        {
                            if (file != null && file.ContentLength > 0)
                            {
                                var fileName = Path.GetFileName(file.FileName);
                                //var filePath = Path.Combine(Server.MapPath(base.Service.UploadFilePath), fileName);
                                string fielSavePath = FileSavePath(base.Service.UploadFilePath, appCreationResponse.JobApplicationNo, fileName, LogUtil);
                                // SAVE THE FILE TO A DIRECTORY FROM WHICH IT CAN BE ACCESED IN NAV and attach each to the application.

                                context.Attachments.Add(new Attachment
                                {
                                    FilePath = fielSavePath,
                                    MimeType = file.ContentType,
                                    JobAppliedId = JobAppliedId
                                });
                                attachments.Add(fielSavePath);
                                try
                                {
                                    file.SaveAs(fielSavePath);
                                }
                                catch (Exception e)
                                {
                                    LogUtil.Logger("Apply2", e.Message);
                                }
                            }
                        }
                        
                        List<string> SectionsfileAttached = new List<string>();
                        // Get the sections of file upload that files were attached to.
                        int od = int.Parse(model.OtherDocument);
                        for (int i = 0; i < model.FileAttached.Count; i++)
                        {
                            if (model.FileAttached[i])
                            {
                                string value = i == 0 ? "CV/RESUME" : i == 1 ? "PHOTO" : model.DocumentTypes[od];
                        SectionsfileAttached.Add(value);
                            }
                        }
                                                
                        // Add the attchments path to NAV.
                        Service.AddJobApplicationAttachments(attachments, appCreationResponse.JobApplicationNo, SectionsfileAttached);
                    }
                    try
                    {
                        // Write uploaded file information to the database.
                        context.SaveChanges();

                        // Update the database with related referee, employment history, Qualifications information.
                        User.Identity.UpdateRecruitmentDetails(model, base.Service, context);
                    }
                    catch (Exception e)
                    {
                        LogUtil.Logger("Apply2", e.ToString());
                    }

                    // Send Succes mail to the applicant.
                    Service.MailNotifyJobApplicants(appCreationResponse.JobApplicationNo);
                    return View("ApplicationSubmitted");
                }
                else
                {
                    //Danger("Application not submitted. Error: " + appCreationResponse.ErrorMessage);
                    Danger("An error occured and the application not submitted. Please check your entries or try again.");
                }
            }
            AttachSelectListItems(model.ApplicantBioData);
            //ViewBag.LanguageSelectList = base.Service.LangugeTypesSelectList(Service.LanguageCode, "");
            foreach (var item in model.Qualifications)
            {
                AttachSelectListItems(item);
            }
            foreach (var item in model.Languages)
            {
                AttachLanguageSelectList(item);
            }
            return View(model);
        }

        [Route("requirement")]
        [AllowAnonymous]
        public ActionResult JobRequirement(string JobId)
        {
            base.Init();
            ViewBag.IsSupervisor = base.UserIsSupervisor();
            JobRequirementViewModel model = new JobRequirementViewModel();
            List<Vacancy> vacancies = new List<Vacancy>();
            // Determine if the currently logged in user is an Employee and display the appropriate Vacancy listing.
            if (Request.IsAuthenticated && User.IsInRole(employeeRole))
                vacancies = Service.GetVacancies(true);
            else
                vacancies = Service.GetVacancies(false);
            model.Vacancies = vacancies;

            model.SelectedVacancy = vacancies.Where(a => a.Job_ID == JobId).First();

            var jobRequirement = base.Service.GetJobRequirement(JobId);
            model.JobRequirement = jobRequirement.Job_Req.GroupBy(a => a.Qualification_Type);
            return View(model);
        }

        public ActionResult ApplicationDetail(string ApplicationNo)
        {
            base.Init();
            ViewBag.IsSupervisor = base.UserIsSupervisor();
            JobApplicationDetail model = base.Service.GetJobApplication(ApplicationNo);
            return View(model);
        }


        // This is used to generate templates to add extra data sections for referee,hobbies or qualifications.
        public JsonResult RenderTemplateToHtml(string Template, int Index)
        {
            base.Init();
            string htmlTemplate = "";

            // Three templates for now to select between referee,hobbies or qualifications
            switch (Template)
            {
                case "referees":
                    htmlTemplate = "~/Views/Recruitment/_RefereeInsertTemplate.cshtml";
                    break;

                case "employment":
                    htmlTemplate = "~/Views/Recruitment/_EmploymentInsertTemplate.cshtml";
                    break;

                case "qualifications":
                    htmlTemplate = "";
                    break;
                default:
                    break;
            }
            HtmlFieldIndexer model = new HtmlFieldIndexer { Index = Index };
            var data = new { htmlString = RenderViewToString(ControllerContext, htmlTemplate, model, true) };
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RelatedQualifications(string QualificationType, string CurrentSelection)
        {
            base.Init();
            var data = base.Service.RelatedQualificationCodes(Service.EducationQualificationTypeCode, QualificationType, CurrentSelection);
            // Remove unwanted strings before returning to the view.
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}