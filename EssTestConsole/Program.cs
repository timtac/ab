﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EssPortal.Classes;
using System.Web.Mvc;
namespace EssTestConsole
{
    using EssPortal.JobApplicationRef;
    using EssPortal.Models;
    using PerformanceMgmtLine;
    using System.Globalization;

    class Program
    {
        public static NAVServiceWrapper navServiceWrapper { get; set; }

        private static void Init()
        {
            navServiceWrapper = new NAVServiceWrapper();
            navServiceWrapper.NavServer = "TECHPRO10-PC";
            navServiceWrapper.NavPort = "1025";
            navServiceWrapper.NavInstance = "ACCESS";
            navServiceWrapper.NavCompany = "AB%20Microfinance%20Bank";
            navServiceWrapper.NavUser = @"TECHPRO10-PC\TECHPRO 10";
            navServiceWrapper.NavPwd = "coretec1";
            navServiceWrapper.UseDefaultCredential = true;
            navServiceWrapper.LogUtil = new LogUtil { FilePathLOG = @"C:\ess-test-log\" };
            navServiceWrapper.DateFormat = "dd/MM/yyyy";
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Process Started");

            ///GetVacanciesTest();
            //IsEmployeeTest("ah@cronus-demosite.com");
            //GetLeaveApplicationsTest();
            //GetHistoricalLeaveRecordsTest();
            //SubmitLeaveApplicationTest();
            //PendingLeaveToApproveTest();
            //MailEmailConfirmationTest();
            //MailNotifyLeaveApproverTest();
            //GetEmployeeAppraisalTest();
            //AddJobApplicationAttachmentsTest();
            //PerformanceLineTest();
            //PerformaceRatingsTest();
            //GetBehaviouralTemplateTest();
            //InsertAppraisal();
            //GetWorkingDaysTest();
            //GetHrSetUpTest();
            //GetApprovalUserSetUpTest();
            //MailNotifyAppraiseeTest();
            //DateWrite();
            //GetSupervisorApprovedLeaveApplicationsTest();
            //LangugeTypesSelectListTest();
            KeyIdTest();
            Console.WriteLine("Process Ended");

            Console.ReadKey();
        }

        public static void KeyIdTest()
        {
            JobApplication_Service service = new JobApplication_Service();
            service.UseDefaultCredentials = true;
            service.Url = "http://TECHPRO10-PC:1092/accessDB/WS/AB%20Microfinance%20Bank/Page/JobApplication";
            try
            {
                var items = service.ReadMultiple(null, null, 1);
                foreach (var item in items)
                {
                    Console.WriteLine("{0}", item.Key);
                    Console.WriteLine("{0}", item.Job_Application_No);
                    Console.WriteLine("{0}", item.ID_Number);
                    Console.WriteLine("{0}", "-----------------");
                    Console.WriteLine("{0}", service.GetRecIdFromKey(item.Key));
                    Console.WriteLine("{0}", service.ReadByRecId(service.GetRecIdFromKey(item.Key)).Job_Application_No);

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }

        public static void DateWrite()
        {
            DateTime date = new DateTime(1900,01,01);
            DateTime.TryParse("01/01/2016", out date);
            Console.WriteLine(date.ToString());
            DateTime.TryParseExact("    ", "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date);
            Console.WriteLine(date.ToString());
        }

        public static void GetApprovalUserSetUpTest()
        {
            Init();
            var t = navServiceWrapper.GetApprovalUserSetUp(@"TECHPRO10-PC\GUEST");
            Console.WriteLine(t.Approver_ID);
        }

        public static void LangugeTypesSelectListTest()
        {
            Init();
            //var t = navServiceWrapper.LangugeTypesSelectList("LANGUAGE", "");
            //Console.WriteLine(t);
        }

        public static void GetSupervisorApprovedLeaveApplicationsTest()
        {
            Init();
            var t = navServiceWrapper.GetSupervisorApprovedLeaveApplications("test@yahoo.com");
            Console.WriteLine(t.Success);
            Console.WriteLine(t.Leave.Count);
        }
        
        public static void MailNotifyAppraiseeTest()
        {
            Init();
            navServiceWrapper.MailNotifyAppraisee("APP-0027");
        }

        public static void GetHrSetUpTest()
        {
            Init();
                Console.WriteLine(navServiceWrapper.GetHrSetUp().Min_Leave_App_Months);
        }

        public static void GetWorkingDaysTest()
        {
            Init();
            Console.WriteLine(navServiceWrapper.GetWorkingDays(DateTime.Today, 6, false));
        }

        public static void InsertAppraisal()
        {
            Init();
            navServiceWrapper.InsertAppraisal(@"TECHPRO10-PC\TECHPRO 10");
        }


        public static void PerformanceLineTest()
        {
            PerformanceMgmtLine_Service service = new PerformanceMgmtLine_Service();
            service.UseDefaultCredentials = true;
            service.Url = "http://TECHPRO10-PC:3458/AccessS2/WS/AB%20Microfinance%20Bank%20LTD/Page/PerformanceMgmtLine";
            try
            {
                var items = service.ReadMultiple(null, null, 0);
                foreach (var item in items)
                {
                    Console.WriteLine("{0}", item.Appraisal_No);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            
        }

        public static void GetVacanciesTest()
        {
            Init();
            var result = navServiceWrapper.GetVacancies(true);
            foreach (var item in result)
            {
                Console.WriteLine("{0} -- {1}", item.Requisition_No, item.Job_description);
            }
        }

        public static void PerformaceRatingsTest()
        {
            Init();
            var result = navServiceWrapper.PerformaceRatings();
            foreach (var item in result.OrderByDescending(a=>a.Score))
            {
                Console.WriteLine("{0} -- {1}", item.Rating, item.Score);
            }
        }

        public static void GetLeaveApplicationsTest()
        {
            Init();
            var result = navServiceWrapper.GetLeaveApplications("E0010");
            if (result.Success)
            {
                foreach (var item in result.Leave)
                {
                    Console.WriteLine("{0} -- {1}", item.Application_Code, item.Status);
                }
            }
            else
                Console.WriteLine("{0}", result.ErrorMessage);

        }

        public static void GetHistoricalLeaveRecordsTest()
        {
            Init();
            var result = navServiceWrapper.GetHistoricalLeaveRecords("E0010");
            if (result.Success)
            {
                foreach (var item in result.LeaveHistory)
                {
                    Console.WriteLine("{0} -- {1}", item.Application_Code, item.Status);
                }
            }
            else
                Console.WriteLine("{0}", result.ErrorMessage);
        }

        public static void IsEmployeeTest(string email)
        {
            Init();
            var isEmployee = navServiceWrapper.IsEmployee(email);
            if (isEmployee.IsEmployee)
                Console.WriteLine("User is an employee");
            else
                Console.WriteLine("User is not an employee");
        }
        
        public static void SubmitLeaveApplicationTest()
        {
            Init();
            LeaveApplicationViewModel model = new LeaveApplicationViewModel();
            model.EmployeeNo = "E0030";
            model.LeaveDaysAppliedFor = "6";
            model.LeaveStartDate = DateTime.Now.AddDays(1).ToString(navServiceWrapper.DateFormat);

            var result = navServiceWrapper.SubmitLeaveApplication(model, EssPortal.LeaveRef.Status.New);
            if (result.Success)
            {
                Console.WriteLine("{0} -- {1}", "Application Submitted", "Succesfully");
            }
            else
                Console.WriteLine("{0}", result.ErrorMessage);
        }

        public static void PendingLeaveToApproveTest()
        {
            Init();
            var result = navServiceWrapper.PendingLeaveToApprove(@"TPRO\SALAM");
            if (result.Success)
            {

                Console.WriteLine("{0} -- {1}", "Application Submitted", result.Leave.Count);
            }
            else
                Console.WriteLine("{0}", result.ErrorMessage);
        }

        public static void MailEmailConfirmationTest()
        {
            Init();
            navServiceWrapper.MailEmailConfirmation("Link", "simplywhiz@gmail.com");
        }

        public static void MailNotifyLeaveApproverTest()
        {
            Init();
            navServiceWrapper.MailNotifyLeaveApprover("LV0010");
        }

        public static void GetEmployeeAppraisalTest()
        {
            Init();
            foreach (var item in navServiceWrapper.GetEmployeeAppraisals("E0030").Appraisals)
            {
                Console.WriteLine("{0} ----- {1} ------ {2}", item.Appraisal_No, item.Appraisal_Period, item.Appraisal_No);
            }
            
        }

        public static void AddJobApplicationAttachmentsTest()
        {
            Init();
           //navServiceWrapper.AddJobApplicationAttachments(new List<string>() { "123", "456", "7889" }, "JOBAPP021", "cover letter");

        }

        public static void GetBehaviouralTemplateTest()
        {
            Init();
            foreach (var item in navServiceWrapper.GetBehaviouralTemplate(true).TemplateGroup)
            {
                Console.WriteLine("{0} ", item.GroupName);
                Console.WriteLine("----------------------------------------------------------------------");
                foreach (var a in item.GroupMembers)
                {
                    Console.WriteLine("{0} -- {1} ", a.Objective, a.Description);
                    Console.WriteLine("");
                }
                Console.WriteLine("----------------------------------------------------------------------");
            }
        }
    }
}
